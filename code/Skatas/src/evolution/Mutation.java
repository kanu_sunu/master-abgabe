package evolution;

import java.util.Random;

@FunctionalInterface
public interface Mutation {

	public double getIntensity(int currentGeneration, int generationAmount, int ruleNumber, int ruleAmount, long seed);

	public static Mutation noMutation() {
		return (kg, ng, k, n, s) -> 0;
	}

	public static Mutation decrementalMutation() {
		return (kg, ng, k, n, s) -> (ng - kg) / Double.valueOf(ng);
	}

	public static Mutation sharedMutation() {
		return (kg, ng, k, n, s) -> k == 0 ? 0 : n / Double.valueOf(k);
	}

	public static Mutation randomMutation() {
		return (kg, ng, k, n, s) -> new Random(s).nextDouble();
	}

}
