package evolution;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

import adaption.Rule;
import adaption.RuleSet;
import util.Lists;
import util.Zipper;

@FunctionalInterface
public interface Recombination {

    Individual combine(Individual father, Individual mother, long seed);

    public default Stream<Individual> recombine(Stream<Individual> individuals, long seed) {
        Random r = new Random(seed);
        List<Individual> fathers = Lists.of(individuals);
        List<Individual> mothers = Lists.of(fathers);
        Collections.shuffle(mothers, r);
        return Zipper.zipWith(fathers.stream(), mothers.stream(), r.longs(), this::combine);
    }

    public static Recombination noRecombination() {
        return (father, mother, seed) -> father;
    }

    public static Recombination addingRecombination() {
        return (father, mother, seed) -> new Individual(father.getRuleSet().addRules(mother.getRuleSet()),
                father.getAdaption());
    }

    public static Recombination splitRecombination() {
        return (father, mother, seed) -> {
            List<Rule> fatherL = Lists.of(father.getRuleSet().getRules());
            List<Rule> motherL = Lists.of(mother.getRuleSet().getRules());
            List<Rule> halfFather = fatherL.subList(0, fatherL.size() / 2);
            List<Rule> halfMother = motherL.subList(motherL.size() / 2, motherL.size());
            return new Individual(new RuleSet(Stream.concat(halfFather.stream(), halfMother.stream())),
                    father.getAdaption());
        };
    }

    public static Recombination randomRecombination() {
        return (father, mother, seed) -> {
            Random r = new Random(seed);
            List<Rule> fatherL = Lists.of(father.getRuleSet().getRules());
            List<Rule> motherL = Lists.of(mother.getRuleSet().getRules());
            Collections.shuffle(fatherL, r);
            Collections.shuffle(motherL, r);
            List<Rule> halfFather = fatherL.size() == 0 ? Lists.empty() : fatherL.subList(0, r.nextInt(fatherL.size()));
            List<Rule> halfMother = motherL.size() == 0 ? Lists.empty() : motherL.subList(0, r.nextInt(motherL.size()));
            return new Individual(new RuleSet(Stream.concat(halfFather.stream(), halfMother.stream())),
                    father.getAdaption());
        };
    }

}
