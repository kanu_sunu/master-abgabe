package evolution;

import adaption.Adaption;
import adaption.RuleSet;
import util.Lazy;

public class Individual implements Comparable<Individual> {

    private final RuleSet ruleset;
    private final Lazy<Double> fitness;
    private final Adaption adaption;

    public Individual(RuleSet ruleset, Adaption adaption) {
        this.ruleset = ruleset;
        this.fitness = new Lazy<>(() -> adaption.getFitness(ruleset));
        this.adaption = adaption;
    }

    public static Individual random(int ruleAmount, Adaption adaption, long seed) {
        return new Individual(RuleSet.random(ruleAmount, seed), adaption);
    }

    public RuleSet getRuleSet() {
        return this.ruleset;
    }

    public double getFitness() {
        return this.fitness.get();
    }

    public Adaption getAdaption() {
        return this.adaption;
    }

    public Individual mutate(Mutation mutation, int currentGeneration, int generationAmount, long seed) {
        return new Individual(this.ruleset.mutate(mutation, currentGeneration, generationAmount, seed), this.adaption);
    }

    @Override
    public String toString() {
        return this.ruleset.toString();
    }

    public Individual simplify() {
        return new Individual(new RuleSet(this.ruleset.getRules().filter(rule -> rule.getActivations() > 0)), this.adaption);
    }

    @Override
    public int compareTo(Individual other) {
        return Double.valueOf(this.getFitness()).compareTo(other.getFitness());
    }

}
