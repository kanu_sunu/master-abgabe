package evolution;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import adaption.Adaption;
import adaption.RuleSet;
import util.Lazy;
import util.Lists;

public class Generation {

    private final List<Individual> individuals = Lists.empty();
    private Lazy<Individual> best;

    public Generation(Stream<Individual> individuals) {
        individuals.forEach(this.individuals::add);
        this.best = new Lazy<>(() -> this.individuals.parallelStream().collect(Collectors.maxBy(Individual::compareTo)).get());
    }

    public Individual getBestIndividual() {
        return this.best.get();
    }

    public Generation nextGeneration(Mutation mutation, Selection selection, Recombination recombination, int genSize, int currentGen,
            int genAmount, long seed) {
        Random r = new Random(seed);
        this.individuals.parallelStream().forEach(i -> i.getFitness());
        List<Individual> selected = Lists.of(selection.select(this.individuals.stream(), genSize, r.nextLong()));
        List<Individual> children = Lists.of(recombination.recombine(selected.stream(), r.nextLong()));
        children.addAll(selected);
        Collections.shuffle(children, r);
        Stream<Individual> newIndividuals = children.stream().map(child -> child.mutate(mutation, currentGen, genAmount, r.nextLong()));
        return new Generation(newIndividuals);
    }

    @Override
    public String toString() {
        return this.individuals.toString();
    }

    public static Generation emptyGeneration(int generationSize, Adaption adaption) {
        return new Generation(Stream.generate(() -> new Individual(RuleSet.EMPTY, adaption)).limit(generationSize));
    }

    public static Generation identicalGeneration(int generationSize, Adaption adaption, RuleSet first) {
        return new Generation(Stream.generate(() -> new Individual(first, adaption)).limit(generationSize));
    }

    public static Generation randomGeneration(int generationSize, int ruleAmount, Adaption adaption, long seed) {
        return new Generation(new Random(seed).longs().mapToObj(s -> Individual.random(ruleAmount, adaption, s)).limit(generationSize));
    }
}
