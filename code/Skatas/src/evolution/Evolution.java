package evolution;

import java.util.Random;

public final class Evolution {

    public static Individual start(Mutation mutation, Selection selection, Recombination recombination, Generation first,
            int generationSize, int generationAmount, long seed) {

        Random r = new Random(seed);
        Generation current = first;
        Individual bestIndividual = current.getBestIndividual();

        for (int currentGeneration = 0; currentGeneration < generationAmount; currentGeneration++) {
            current = current.nextGeneration(mutation, selection, recombination, generationSize, currentGeneration, generationAmount,
                    r.nextLong());
            Individual currentlyBest = current.getBestIndividual();
            if (currentlyBest.getFitness() > bestIndividual.getFitness()) {
                bestIndividual = currentlyBest;
            }
        }
        return bestIndividual;
    }
}
