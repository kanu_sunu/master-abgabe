package evolution;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import tuple.T2;
import util.Lists;

@FunctionalInterface
public interface Selection {

    public Stream<Individual> select(Stream<Individual> individuals, int generationSize, long seed);

    public static Selection noSelection() {
        return (individuals, generationSize, seed) -> {
            List<Individual> result = Lists.of(individuals);
            Collections.shuffle(result, new Random(seed));
            return result.stream().limit(generationSize);
        };
    }

    public static Selection onlyBestSelection() {
        return (individuals, generationSize, seed) -> individuals.sorted().limit(generationSize);
    }

    public static Selection fitnessSelection() {
        return (individuals, generationSize, seed) -> {
            List<Individual> all = Lists.of(individuals);

            double sumOfFitnesses = all.stream().collect(Collectors.summingDouble(Individual::getFitness));
            if (sumOfFitnesses == 0) {
                return noSelection().select(all.stream(), generationSize, seed);
            }

            List<T2<Individual, Double>> probs = Lists.empty();
            double start = 0;

            for (Individual ind : all) {
                double prob = ind.getFitness() / sumOfFitnesses;
                probs.add(new T2<>(ind, start + prob));
                start += prob;
            }

            List<Individual> results = Lists.empty();
            Random r = new Random(seed);
            for (int i = 0; i < generationSize; i++) {
                double rnd = r.nextDouble();
                for (T2<Individual, Double> prob : probs) {
                    if (rnd <= prob.getB()) {
                        results.add(prob.getA());
                        break;
                    }
                }
            }

            return results.stream();
        };
    }

    public static Selection hybridSelection() {
        return (individuals, generationSize, seed) -> {
            List<Individual> all = Lists.of(individuals);
            int fittest = generationSize / 2;
            int others = generationSize - fittest;
            Stream<Individual> fittestOnes = onlyBestSelection().select(all.stream(), fittest, seed);
            Stream<Individual> otherOnes = noSelection().select(all.stream(), others, seed);
            return Stream.concat(fittestOnes, otherOnes);

        };
    }

}
