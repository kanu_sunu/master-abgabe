package util;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import function.F1;

public enum Lists {

    ;

    public static <A> List<A> empty() {
        return new LinkedList<>();
    }

    @SuppressWarnings("unchecked")
    public static <A> List<A> of(A... elements) {
        List<A> result = new ArrayList<>(elements.length);
        for (A element : elements) {
            result.add(element);
        }
        return result;
    }

    public static <A> List<A> of(Stream<A> stream) {
        return stream.collect(Collectors.toList());
    }

    public static <A> List<A> of(List<A> list) {
        return of(list.stream());
    }

    public static <A> boolean or(List<A> list, F1<A, Boolean> pred) {
        for (A a : list) {
            if (pred.apply(a)) {
                return true;
            }
        }
        return false;
    }

}
