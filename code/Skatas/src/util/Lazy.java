package util;

import java.util.function.Supplier;

public class Lazy<A> {

    private A a;
    private Supplier<A> func;

    public Lazy(Supplier<A> func) {
        this.a = null;
        this.func = func;
    }

    public A get() {
        if (this.a == null) {
            this.a = this.func.get();
            this.func = null;
        }
        return this.a;
    }

}
