package util;

import java.util.Iterator;
import java.util.Objects;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.BiFunction;
import java.util.stream.BaseStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public enum Zipper {
    ;

    public static <A, B, R> Stream<R> zipWith(BaseStream<A, ?> aStream, BaseStream<B, ?> bStream, BiFunction<A, B, R> func) {
        Spliterator<A> aSpliterator = Objects.requireNonNull(aStream).spliterator();
        Spliterator<B> bSpliterator = Objects.requireNonNull(bStream).spliterator();

        int characteristics = aSpliterator.characteristics() & bSpliterator.characteristics()
                & ~(Spliterator.DISTINCT | Spliterator.SORTED);

        long zipSize = ((characteristics & Spliterator.SIZED) != 0)
                ? Math.min(aSpliterator.getExactSizeIfKnown(), bSpliterator.getExactSizeIfKnown()) : -1;

        Iterator<A> aIterator = Spliterators.iterator(aSpliterator);
        Iterator<B> bIterator = Spliterators.iterator(bSpliterator);
        Iterator<R> cIterator = new Iterator<R>() {
            @Override
            public boolean hasNext() {
                return aIterator.hasNext() && bIterator.hasNext();
            }

            @Override
            public R next() {
                return func.apply(aIterator.next(), bIterator.next());
            }
        };

        Spliterator<R> split = Spliterators.spliterator(cIterator, zipSize, characteristics);
        return StreamSupport.stream(split, (aStream.isParallel() || bStream.isParallel()));
    }

    public static <A, B, C, R> Stream<R> zipWith(BaseStream<A, ?> aStream, BaseStream<B, ?> bStream, BaseStream<C, ?> cStream,
            TriFunction<A, B, C, R> func) {
        Spliterator<A> aSpliterator = Objects.requireNonNull(aStream).spliterator();
        Spliterator<B> bSpliterator = Objects.requireNonNull(bStream).spliterator();
        Spliterator<C> cSpliterator = Objects.requireNonNull(cStream).spliterator();

        int characteristics = aSpliterator.characteristics() & bSpliterator.characteristics() & cSpliterator.characteristics()
                & ~(Spliterator.DISTINCT | Spliterator.SORTED);

        long zipSize = ((characteristics & Spliterator.SIZED) != 0) ? Math.min(
                Math.min(aSpliterator.getExactSizeIfKnown(), bSpliterator.getExactSizeIfKnown()), cSpliterator.getExactSizeIfKnown()) : -1;

        Iterator<A> aIterator = Spliterators.iterator(aSpliterator);
        Iterator<B> bIterator = Spliterators.iterator(bSpliterator);
        Iterator<C> cIterator = Spliterators.iterator(cSpliterator);
        Iterator<R> rIterator = new Iterator<R>() {
            @Override
            public boolean hasNext() {
                return aIterator.hasNext() && bIterator.hasNext() && cIterator.hasNext();
            }

            @Override
            public R next() {
                return func.apply(aIterator.next(), bIterator.next(), cIterator.next());
            }
        };

        Spliterator<R> split = Spliterators.spliterator(rIterator, zipSize, characteristics);
        return StreamSupport.stream(split, (aStream.isParallel() || bStream.isParallel() || cStream.isParallel()));
    }

    public static <A, B> Stream<P<A, B>> zip(BaseStream<A, ?> aStream, BaseStream<B, ?> bStream) {
        return zipWith(aStream, bStream, (a, b) -> new P<>(a, b));
    }
}
