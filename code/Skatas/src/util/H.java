package util;

import java.util.function.BiFunction;
import java.util.stream.BaseStream;
import java.util.stream.Stream;

public enum H {

    ;

    public static <A, B, R> Stream<R> zipWith(BaseStream<A, ?> aStream, BaseStream<B, ?> bStream,
            BiFunction<A, B, R> func) {
        return Zipper.zipWith(aStream, bStream, func);
    }

    public static <A, B, C, R> Stream<R> zipWith(BaseStream<A, ?> aStream, BaseStream<B, ?> bStream,
            BaseStream<C, ?> cStream, TriFunction<A, B, C, R> func) {
        return Zipper.zipWith(aStream, bStream, cStream, func);
    }

    public static <A, B> Stream<P<A, B>> zip(BaseStream<A, ?> aStream, BaseStream<B, ?> bStream) {
        return Zipper.zip(aStream, bStream);
    }

}
