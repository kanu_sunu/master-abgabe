package util;

import java.util.function.BiFunction;
import java.util.function.Function;

public class P<A, B> {

    final A a;
    final B b;

    P(A a, B b) {
        this.a = a;
        this.b = b;
    }

    public A getA() {
        return this.a;
    }

    public B getB() {
        return this.b;
    }

    public <R> P<R, B> mapA(Function<A, R> mapper) {
        return new P<>(mapper.apply(this.a), this.b);
    }

    public <R> P<A, R> mapB(Function<B, R> mapper) {
        return new P<>(this.a, mapper.apply(this.b));
    }

    public <R, S> P<R, S> map(BiFunction<A, B, P<R, S>> mapper) {
        return mapper.apply(this.a, this.b);
    }

}
