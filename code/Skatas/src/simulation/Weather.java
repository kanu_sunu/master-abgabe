package simulation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public enum Weather {

	;

	private static final Map<Integer, Double> WIND = fillMap("rsc/wind.txt");
	private static final Map<Integer, Double> SUN = fillMap("rsc/sun.txt");
	private static final Map<Integer, Double> TEMPERATURE = fillMap("rsc/temperature.txt");

	private static Map<Integer, Double> fillMap(String path) {
		Map<Integer, Double> result = new HashMap<>();
		int row = 0;
		try {
			for (String line : Files.readAllLines(Paths.get(path))) {
				result.put(row++, Double.valueOf(line));
			}
		} catch (IOException e) {
			System.err.println("Error initiating Map - IOException: " + e.getMessage());
		}
		return result;
	}

	private static Optional<Double> extract(LocalDateTime time, Map<Integer, Double> map) {
		int day = time.getDayOfYear() < 366 ? time.getDayOfYear() : 365;
		int hour = time.getMinute() < 30 ? time.getHour() : time.getHour() + 1;
		int row = (day - 1) * 24 + hour;
		Double value1 = map.get(row);
		Double value2 = map.get(time.getMinute() < 30 ? row + 1 : row - 1);
		Double between = interpolate(value1, value2, time.getMinute());
		return between == null ? Optional.empty() : Optional.of(between);
	}

	private static Double interpolate(Double a, Double b, int minute) {
		if (a == null) {
			return b;
		} else if (b == null) {
			return a;
		}
		double factor = minute / 60.0;
		return a < b ? a + factor * (b - a) : b + factor * (a - b);
	}

	public static Optional<Double> sun(LocalDateTime time) {
		return extract(time, SUN);
	}

	public static Optional<Double> wind(LocalDateTime time) {
		return extract(time, WIND);
	}

	public static Optional<Double> temperature(LocalDateTime time) {
		return extract(time, TEMPERATURE);
	}
}