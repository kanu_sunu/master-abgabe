package simulation;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import adaption.Rule;
import adaption.RuleSet;
import adaption.trigger.ThresholdTrigger;
import adaption.trigger.TimeTrigger;
import simulation.Component.NAME;
import simulation.components.CoolingMachine;
import simulation.components.EnergyStorage;
import simulation.components.Photovoltaics;
import simulation.components.Storage;
import util.Lazy;
import util.Lists;

public class Situation {

    private final LocalDateTime moment;
    private final Photovoltaics pv;
    private final List<Component> components = Lists.empty();
    private final Lazy<Double> consumption;
    private final double milk;
    private final double milkTemp;
    private final RuleSet rules;
    private final Map<NAME, Double> consumptions = new HashMap<>();
    private final double energyLoaded;

    public Situation(LocalDateTime moment, Photovoltaics pv, Stream<Component> components, double milk, double milkTemp, RuleSet rules) {
        this.moment = moment;
        this.pv = pv;
        this.milk = milk;
        this.milkTemp = milkTemp;
        this.rules = rules;
        components.forEach(this.components::add);
        this.consumption = new Lazy<>(
                () -> this.components.stream().collect(Collectors.summingDouble(c -> c.getConsumption(this.pv, this.milk > 0))));
        this.components.stream().forEach(c -> this.consumptions.put(c.getName(), c.getConsumption(this.pv, this.milk > 0)));
        this.energyLoaded = Math.max(0,
                ((EnergyStorage) this.components.stream().filter(c -> c instanceof EnergyStorage).findFirst().get()).getEnergy());
    }

    public Situation stepForward(int minutes) {
        LocalDateTime newDate = this.moment.plusMinutes(minutes);
        Photovoltaics newPv = this.pv.delayBy(minutes);
        Stream<Component> newComponents = this.components.stream().map(c -> c.stepForward(minutes, newPv, this.milk > 0));
        Situation newSituation = new Situation(newDate, newPv, newComponents, this.milk, this.milkTemp, this.rules);
        return newSituation.applyRules(Simulation.STANDARD_RULESET).applyRules(this.rules).handleMilk(minutes);
    }

    private Situation applyRules(RuleSet newRules) {
        Map<NAME, Rule> affectations = new HashMap<>();
        newRules.getRules().forEach(rule -> {
            for (Component c : this.components) {
                // Component affected = rule.affect(c, this);
                if (rule.affects(c, this)) {
                    Rule before = affectations.get(c.getName());
                    affectations.put(c.getName(), newRuleBasedOnTrigger(rule, before));
                }
            }
        });
        Collection<Component> newComponents = Lists.empty();
        for (Component c : this.components) {
            Rule stored = affectations.get(c.getName());
            if (stored != null) {
                stored.use();
            }
            newComponents.add(stored == null ? c : stored.affect(c, this));
        }
        return new Situation(this.moment, this.pv, newComponents.stream(), this.milk, this.milkTemp, this.rules);

    }

    private Rule newRuleBasedOnTrigger(Rule rule, Rule before) {
        if (before == null) {
            return rule;
        } else if (before.getTrigger() instanceof ThresholdTrigger) {
            return rule.getTrigger() instanceof TimeTrigger ? rule : evalBetterTrigger(before, rule);
        } else {
            return before;
        }
    }

    private Rule evalBetterTrigger(Rule before, Rule rule) {
        ThresholdTrigger beforeTrigger = (ThresholdTrigger) before.getTrigger();
        double oldThreshold = beforeTrigger.getThreshold();

        ThresholdTrigger ruleTrigger = (ThresholdTrigger) rule.getTrigger();
        double ruleThreshold = ruleTrigger.getThreshold();

        return Math.abs(oldThreshold - this.getConsumption()) > Math.abs(ruleThreshold - this.getConsumption()) ? rule : before;
    }

    private Situation handleMilk(int minutes) {
        double incomingMilk = this.components.stream().collect(Collectors.summingDouble(c -> c.getMilkFlow(minutes)));
        Stream<Component> newComponents = this.components.stream();
        double newMilk = this.milk;
        double finalMilkTemp = this.milkTemp;

        if (this.milk > 0 || incomingMilk > 0) {
            boolean preCooling = Lists.or(this.components, Component::doesPreCooling);
            double incomingMilkTemp = Milk.incomingMilkTemp(preCooling);
            newMilk = this.milk + incomingMilk;
            double newMilkTemp = Milk.mergeTemps(this.milk, this.milkTemp, incomingMilk, incomingMilkTemp);
            boolean activeCooler = Lists.or(this.components, c -> c instanceof CoolingMachine && c.getConsumption(null, true) > 0);
            finalMilkTemp = activeCooler ? Milk.tempAfterCooling(newMilk, newMilkTemp, minutes) : newMilkTemp;

            if (incomingMilk == 0 && this.milkTemp <= Milk.COOLED_MILK_TEMP) {
                newMilk = 0;
                finalMilkTemp = 0;
                newComponents = this.components.stream().map(c -> c instanceof CoolingMachine ? c.setActivity(false) : c);
            }
        }
        return new Situation(this.moment, this.pv, newComponents, newMilk, finalMilkTemp, this.rules);

    }

    public double getPvConsumption() {
        return this.pv.getPower();
    }

    public double getConsumption() {
        return this.consumption.get();
    }

    public Map<NAME, Double> getConsumptions() {
        return this.consumptions;
    }

    public double getLoadedEnergy() {
        return this.energyLoaded;
    }

    public LocalDateTime getMoment() {
        return this.moment;
    }

    public Stream<Component> getComponents() {
        return this.components.stream();
    }

    public double getEnergyLeft() {
        return this.components.stream().filter(c -> c instanceof EnergyStorage)
                .collect(Collectors.summingDouble(c -> ((Storage) c).getEnergy()));
    }

    public double getMilkTemp() {
        return this.milkTemp;
    }
}