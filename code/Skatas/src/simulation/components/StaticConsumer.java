package simulation.components;

import simulation.Component;

public class StaticConsumer extends Component {

	private final double current;
	private final double milkFlow;

	public StaticConsumer(NAME name, double current) {
		super(false, 1.0, name);
		this.current = current;
		this.milkFlow = 0;
	}

	public StaticConsumer(NAME name, double current, double milkFlow) {
		super(false, 1.0, name);
		this.current = current;
		this.milkFlow = milkFlow;
	}

	private StaticConsumer(boolean active, double efficiency, NAME name, double current, double milkFlow) {
		super(active, efficiency, name);
		this.current = current;
		this.milkFlow = milkFlow;
	}

	@Override
	public double getMilkFlow(int minutes) {
		return this.active ? this.milkFlow * minutes : 0;
	}

	@Override
	public double getPower(Photovoltaics pv, boolean milking) {
		return this.current;
	}

	@Override
	protected Component copy(boolean newActive, double newEfficiency, NAME newName) {
		return new StaticConsumer(newActive, newEfficiency, newName, this.current, this.milkFlow);
	}

}
