package simulation.components;

import simulation.Component;

public interface Storage {

	Component startLoading(double threshold);

	Component startDischarging(double threshold);

	double getEnergy();

}
