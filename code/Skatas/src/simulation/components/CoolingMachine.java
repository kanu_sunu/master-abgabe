package simulation.components;

import simulation.Component;

public class CoolingMachine extends Component {

	private final double current;

	public CoolingMachine(NAME name, double current) {
		super(false, 1.0, name);
		this.current = current;
	}

	private CoolingMachine(boolean active, double efficiency, NAME name, double current) {
		super(active, efficiency, name);
		this.current = current;
	}

	@Override
	public double getPower(Photovoltaics pv, boolean milking) {
		return this.current;
	}

	@Override
	protected Component copy(boolean newActive, double newEfficiency, NAME newName) {
		return new CoolingMachine(newActive, newEfficiency, newName, this.current);
	}

}
