package simulation.components;

import java.util.Optional;

import simulation.Component;
import simulation.Milk;
import simulation.Simulation;

public class CoolnessStorage extends Component implements Storage {

    private static final double MELTING_TEMP_ICE = 333;

    private final double energy; // in kJ
    private final double capacity; // in kJ
    private final double loadingTime; // in min

    private final Optional<Double> loading;
    private final Optional<Double> discharging;

    public CoolnessStorage(NAME name, double weight, double startEnergy, double loadingTime) {
        this(false, 1.0, name, weight * MELTING_TEMP_ICE, loadingTime, startEnergy, Optional.empty(), Optional.empty());
    }

    public CoolnessStorage(boolean active, double efficiency, NAME name, double capacity, double loadingTime,
            double startEnergy, Optional<Double> loading, Optional<Double> discharging) {
        super(active || loading.isPresent() || discharging.isPresent(), efficiency, name);
        this.capacity = capacity;
        this.energy = startEnergy;
        this.loading = this.active ? loading : Optional.empty();
        this.discharging = this.active ? discharging : Optional.empty();
        this.loadingTime = loadingTime;
    }

    @Override
    public Component startLoading(double threshold) {
        return this.energy / this.capacity < threshold ? new CoolnessStorage(true, this.efficiency, this.name,
                this.capacity, this.loadingTime, this.energy, Optional.of(threshold), Optional.empty()) : this;
    }

    @Override
    public Component startDischarging(double threshold) {
        return this.energy / this.capacity > threshold ? new CoolnessStorage(true, this.efficiency, this.name,
                this.capacity, this.loadingTime, this.energy, Optional.empty(), Optional.of(threshold)) : this;
    }

    private static final double DENSITY_ICE = 4.178; // in kJ/(kg * K)
    private static final double ENERGY_PER_MINUTE = Milk.MILK_FLOW_PER_MINUTE / Simulation.MINUTES_PER_TICK
            * Milk.MASS_MILK * DENSITY_ICE * (Milk.WARM_MILK_TEMP - Milk.PRECOOLED_MILK_TEMP); // in kJ

    @Override
    public Component stepForward(int minutes, Photovoltaics pv, boolean milking) {
        Optional<Double> newLoading = this.loading;
        Optional<Double> newDischarging = this.discharging;
        double newEnergy = calculateNewEnergy(minutes, milking);
        double loadingState = newEnergy / this.capacity;
        if (this.loading.isPresent() && loadingState >= this.loading.get()) {
            newLoading = Optional.empty();
        }
        if (this.discharging.isPresent() && loadingState <= this.discharging.get() || !milking) {
            newDischarging = Optional.empty();
        }

        return new CoolnessStorage(newLoading.isPresent() || newDischarging.isPresent(), this.efficiency, this.name,
                this.capacity, this.loadingTime, newEnergy, newLoading, newDischarging);
    }

    private double calculateNewEnergy(int minutes, boolean milking) {
        double newEnergy = this.energy;
        if (this.discharging.isPresent() && milking) {
            newEnergy -= ENERGY_PER_MINUTE * minutes;
        }
        if (this.loading.isPresent()) {
            newEnergy += this.capacity / this.loadingTime * minutes;
        }
        return Math.max(0, Math.min(this.capacity, newEnergy));
    }

    @Override
    public boolean doesPreCooling() {
        return this.discharging.isPresent() && this.energy > 0;
    }

    @Override
    public boolean isStorage() {
        return true;
    }

    private static final double W_KJ_FACTOR = 3.6;

    @Override
    public double getPower(Photovoltaics pv, boolean milking) {
        double result = 0;
        if (this.loading.isPresent()) {
            result += this.capacity / this.loadingTime * 60 / W_KJ_FACTOR;
        }
        if (this.discharging.isPresent() && milking) {
            result += 3510;
        }
        return result;
    }

    @Override
    protected Component copy(boolean newActive, double newEfficiency, NAME newName) {
        return new CoolnessStorage(newActive, newEfficiency, newName, this.capacity, this.loadingTime, this.energy, this.loading,
                this.discharging);
    }

    @Override
    public double getEnergy() {
        return this.energy / W_KJ_FACTOR;
    }

}
