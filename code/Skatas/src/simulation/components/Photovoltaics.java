package simulation.components;

import java.time.LocalDateTime;

import simulation.Weather;

public class Photovoltaics {

    private static final double FACTOR = 1000;
    private static final double DEGREE = 0.20;

    private final double size;
    private final LocalDateTime moment;
    private final double power;

    public Photovoltaics(double size, LocalDateTime moment) {
        this.size = size;
        this.moment = moment;
        this.power = Weather.sun(this.moment).map(s -> s * size * FACTOR * DEGREE).orElse(0.0);
    }

    public Photovoltaics delayBy(int minutes) {
        return new Photovoltaics(this.size, this.moment.plusMinutes(minutes));
    }

    public double getPower() {
        return this.power;
    }

    public double getSize() {
        return this.size;
    }

}
