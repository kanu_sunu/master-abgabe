package simulation.components;

import java.time.LocalTime;

import simulation.Component;

public class WindMachine extends Component {

	private static final double WIND_POWER = 10000;

	private final LocalTime endTime;
	private final LocalTime startTime;
	private final LocalTime currentTime;
	private final double power;

	public WindMachine(NAME name) {
		this(true, 1.0, name, LocalTime.of(6, 0), WIND_POWER, LocalTime.of(12, 0), LocalTime.of(0, 0));
	}

	public WindMachine(boolean active, double efficiency, NAME name, LocalTime startTime, double power,
			LocalTime endTime, LocalTime current) {
		super(active, efficiency, name);
		this.startTime = startTime;
		this.power = power;
		this.endTime = endTime;
		this.currentTime = current;
	}

	@Override
	public Component stepForward(int minutes, Photovoltaics pv, boolean milking) {
		LocalTime nextTime = this.currentTime.plusMinutes(minutes);
		if (nextTime.equals(this.startTime)) {
			return new WindMachine(true, 1.0, this.name, this.startTime, this.power, this.endTime, nextTime);
		} else if (nextTime.equals(this.endTime)) {
			return new WindMachine(false, 1.0, this.name, this.startTime, this.power, this.endTime, nextTime);
		} else {
			return new WindMachine(this.active, this.efficiency, this.name, this.startTime, this.power, this.endTime,
					nextTime);
		}
	}

	@Override
	public double getPower(Photovoltaics pv, boolean milking) {
		return this.power;
	}

	@Override
	protected Component copy(boolean newActive, double newEfficiency, NAME newName) {
		return new WindMachine(newActive, newEfficiency, newName, this.startTime, this.power, this.endTime, this.currentTime);
	}
}
