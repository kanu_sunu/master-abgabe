package simulation.components;

import java.util.Optional;

import simulation.Component;

public class EnergyStorage extends Component implements Storage {

    private final double energy;
    private final double capacity;
    private final double dischargePower;
    private final double loadingVoltage;

    private final Optional<Double> loading;
    private final Optional<Double> discharging;

    public EnergyStorage(NAME name, double capacity, double dischargePower, double loadingVoltage, double startEnergy) {
        this(false, 1.0, name, capacity, dischargePower, loadingVoltage, startEnergy, Optional.empty(), Optional.empty());
    }

    public EnergyStorage(boolean active, double efficiency, NAME name, double capacity, double dischargePower, double loadingVoltage,
            double startEnergy, Optional<Double> loading, Optional<Double> discharging) {
        super(active || loading.isPresent() || discharging.isPresent(), efficiency, name);

        this.capacity = capacity;
        this.dischargePower = dischargePower;
        this.loadingVoltage = loadingVoltage;

        this.energy = Math.max(0, Math.min(startEnergy, capacity));
        this.loading = loading;
        this.discharging = discharging;
    }

    @Override
    public Component startLoading(double threshold) {
        return this.energy / this.capacity < threshold ? new EnergyStorage(true, this.efficiency, this.name, this.capacity,
                this.dischargePower, this.loadingVoltage, this.energy, Optional.of(threshold), Optional.empty()) : this;
    }

    @Override
    public Component startDischarging(double threshold) {
        return this.energy / this.capacity > threshold ? new EnergyStorage(true, this.efficiency, this.name, this.capacity,
                this.dischargePower, this.loadingVoltage, this.energy, Optional.empty(), Optional.of(threshold)) : this;
    }

    @Override
    public Component stepForward(int minutes, Photovoltaics pv, boolean milking) {
        Optional<Double> newLoading = this.loading;
        Optional<Double> newDischarging = this.discharging;
        double newEnergy = calculateNewEnergy(minutes, pv);
        double loadingState = newEnergy / this.capacity;
        if (this.loading.isPresent() && loadingState >= this.loading.get()) {
            newLoading = Optional.empty();
        }
        if (this.discharging.isPresent() && loadingState <= this.discharging.get()) {
            newDischarging = Optional.empty();
        }
        return new EnergyStorage(newLoading.isPresent() || newDischarging.isPresent(), this.efficiency, this.name, this.capacity,
                this.dischargePower, this.loadingVoltage, newEnergy, newLoading, newDischarging);
    }

    private double calculateNewEnergy(int minutes, Photovoltaics pv) {
        double time = minutes / 60.0;
        double newEnergy = this.energy;
        if (this.discharging.isPresent()) {
            newEnergy -= time * this.dischargePower;
        }
        if (this.loading.isPresent()) {
            newEnergy += time * pv.getPower();
        }
        return Math.max(0, Math.min(this.capacity, newEnergy));
    }

    @Override
    public boolean isStorage() {
        return true;
    }

    @Override
    public double getPower(Photovoltaics pv, boolean milking) {
        double result = 0;
        if (this.loading.isPresent()) {
            result += pv.getPower();
        }
        if (this.discharging.isPresent()) {
            result -= Math.min(this.dischargePower, this.energy);
        }
        return result;
    }

    @Override
    protected Component copy(boolean newActive, double newEfficiency, NAME newName) {
        return new EnergyStorage(newActive, newEfficiency, newName, this.capacity, this.dischargePower, this.loadingVoltage, this.energy,
                this.loading, this.discharging);
    }

    @Override
    public double getEnergy() {
        return this.energy;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(this.capacity);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(this.dischargePower);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + ((this.discharging == null) ? 0 : this.discharging.hashCode());
        temp = Double.doubleToLongBits(this.energy);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + ((this.loading == null) ? 0 : this.loading.hashCode());
        temp = Double.doubleToLongBits(this.loadingVoltage);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        EnergyStorage other = (EnergyStorage) obj;
        if (Double.doubleToLongBits(this.capacity) != Double.doubleToLongBits(other.capacity))
            return false;
        if (Double.doubleToLongBits(this.dischargePower) != Double.doubleToLongBits(other.dischargePower))
            return false;
        if (this.discharging == null) {
            if (other.discharging != null)
                return false;
        } else if (!this.discharging.equals(other.discharging))
            return false;
        if (Double.doubleToLongBits(this.energy) != Double.doubleToLongBits(other.energy))
            return false;
        if (this.loading == null) {
            if (other.loading != null)
                return false;
        } else if (!this.loading.equals(other.loading))
            return false;
        if (Double.doubleToLongBits(this.loadingVoltage) != Double.doubleToLongBits(other.loadingVoltage))
            return false;
        return true;
    }

}
