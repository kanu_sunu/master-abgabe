package simulation;

import java.util.Random;

import simulation.components.Photovoltaics;

public abstract class Component {

    public enum NAME {
        ES, // Energy Storage
        CS, // Coolness Storage

        VP1, VP2, // Vacuum Pumps
        CM1, CM2, // Cooling Machines
        H1, H2, // Heating Machines
        B, // Boiler

        WM1, WM2, WM3, WM4 // Wind Machines
    }

    public static final NAME randomStorage(long seed) {
        return new Random(seed).nextBoolean() ? NAME.ES : NAME.CS;
    }

    protected final boolean active;
    protected final double efficiency;
    protected final NAME name;

    protected Component(boolean active, double efficiency, NAME name) {
        this.active = active;
        this.efficiency = efficiency;
        this.name = name;
    }

    public final double getConsumption(Photovoltaics pv, boolean milking) {
        return this.active ? this.efficiency * this.getPower(pv, milking) : 0;
    }

    public final Component setActivity(boolean active) {
        return this.copy(active, this.efficiency, this.name);
    }

    public final Component setEfficiency(double value) {
        return this.copy(this.active, value, this.name);
    }

    public final NAME getName() {
        return this.name;
    }

    // ----- ABSTRACT ----- //

    @SuppressWarnings("unused")
    public Component stepForward(int minutes, Photovoltaics pv, boolean milking) {
        return this;
    }

    @SuppressWarnings("static-method")
    public boolean isStorage() {
        return false;
    }

    @SuppressWarnings({ "static-method", "unused" })
    public double getMilkFlow(int minutes) {
        return 0;
    }

    @SuppressWarnings("static-method")
    public boolean doesPreCooling() {
        return false;
    }

    // ----- PURE ABSTRACT ----- //

    public abstract double getPower(Photovoltaics pv, boolean milking);

    protected abstract Component copy(boolean newActive, double newEfficiency, NAME newName);

}
