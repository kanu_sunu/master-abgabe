package simulation;

import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import simulation.Component.NAME;

/** 4 * 24 = 96 values **/
public class Progression {

    private final List<Double> values = new LinkedList<>();
    private final List<Double> pvValues = new LinkedList<>();
    private final List<Double> energyLeft = new LinkedList<>();
    private final List<Map<NAME, Double>> consumptions;

    public Progression(double[] values, double[] pvValues, double[] energy, List<Map<NAME, Double>> consumptions) {
        addValues(values, this.values);
        addValues(pvValues, this.pvValues);
        addValues(energy, this.energyLeft);
        this.consumptions = consumptions;
    }

    private static void addValues(double[] values, List<Double> list) {
        for (double value : values) {
            list.add(value);
        }
    }

    public Stream<Double> getValues() {
        return this.values.stream();
    }

    public Stream<Double> getPvValues() {
        return this.pvValues.stream();
    }

    public Stream<Double> getConsumptionsFor(NAME component) {
        return this.consumptions.stream().map(map -> map.get(component));
    }

    public Stream<Double> getEnergy() {
        return this.energyLeft.stream();
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        LocalTime time = LocalTime.of(0, 0);
        for (int i = 0; i < this.values.size(); i++) {
            result.append(time).append('\t').append(this.values.get(i)).append('\n');
            time = time.plusMinutes(15);
        }
        result.append("\nEnergy left: " + this.energyLeft.get(Simulation.SITUATIONS_PER_DAY - 1) + "\n");
        return result.toString();
    }

}
