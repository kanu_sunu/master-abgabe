package simulation;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import adaption.Rule;
import adaption.RuleSet;
import adaption.effect.SetActivityEffect;
import adaption.trigger.TimeTrigger;
import simulation.Component.NAME;
import simulation.components.CoolingMachine;
import simulation.components.CoolnessStorage;
import simulation.components.EnergyStorage;
import simulation.components.Photovoltaics;
import simulation.components.StaticConsumer;
import util.Lists;

public enum Simulation {

    ;

    public static final int MINUTES_PER_TICK = 15;
    public static final int SITUATIONS_PER_DAY = 24 * 60 / MINUTES_PER_TICK;

    private static final int PV_SIZE = 20;

    public static Progression simulate(LocalDate date, RuleSet rules, Stream<Component> extraComponents) {
        LocalDateTime time = date.atStartOfDay().minusMinutes(MINUTES_PER_TICK);
        Photovoltaics pv = new Photovoltaics(PV_SIZE, time);
        Stream<Component> components = Stream.concat(STANDARD_COMPONENTS.stream(), extraComponents);
        double[] values = new double[SITUATIONS_PER_DAY];
        double[] pvValues = new double[SITUATIONS_PER_DAY];
        double[] energy = new double[SITUATIONS_PER_DAY];
        List<Map<NAME, Double>> consumptions = new ArrayList<>(SITUATIONS_PER_DAY);
        Situation situation = new Situation(time, pv, components, 0, 0, rules);
        for (int i = 0; i < SITUATIONS_PER_DAY; i++) {
            situation = situation.stepForward(MINUTES_PER_TICK);

            values[i] = situation.getConsumption();
            pvValues[i] = situation.getPvConsumption();
            energy[i] = situation.getEnergyLeft();
            consumptions.add(situation.getConsumptions());
        }
        return new Progression(values, pvValues, energy, consumptions);
    }

    public static final List<Component> STANDARD_COMPONENTS = getStandardComponents();

    private static List<Component> getStandardComponents() {
        Component energy = new EnergyStorage(NAME.ES, 3580, 2000, 53, 0);
        Component cool = new CoolnessStorage(NAME.CS, 164, 0, 360);

        Component vacuumPump1 = new StaticConsumer(NAME.VP1, 3200, Milk.MILK_FLOW_PER_MINUTE);
        Component vacuumPump2 = new StaticConsumer(NAME.VP2, 3200, Milk.MILK_FLOW_PER_MINUTE);
        Component cooling1 = new CoolingMachine(NAME.CM1, 10000);
        Component cooling2 = new CoolingMachine(NAME.CM2, 10000);
        Component heating1 = new StaticConsumer(NAME.H1, 11500);
        Component heating2 = new StaticConsumer(NAME.H2, 13500);
        Component boiler = new StaticConsumer(NAME.B, 2000);

        return Lists.of(energy, cool, vacuumPump1, vacuumPump2, cooling1, cooling2, heating1, heating2, boiler);
    }

    public static final RuleSet STANDARD_RULESET = getStandardRules();

    private static RuleSet getStandardRules() {
        Rule vp11 = newOnOffRule(LocalTime.of(5, 0), LocalTime.of(6, 45), NAME.VP1);
        Rule vp12 = newOnOffRule(LocalTime.of(15, 30), LocalTime.of(17, 30), NAME.VP1);

        Rule vp21 = newOnOffRule(LocalTime.of(5, 0), LocalTime.of(8, 15), NAME.VP2);
        Rule vp22 = newOnOffRule(LocalTime.of(15, 30), LocalTime.of(18, 15), NAME.VP2);

        Rule c1 = newOnRule(LocalTime.of(16, 0), NAME.CM1);
        Rule c2 = newOnRule(LocalTime.of(5, 15), NAME.CM2);

        Rule h11 = newOnOffRule(LocalTime.of(9, 0), LocalTime.of(9, 30), NAME.H1);
        Rule h12 = newOnOffRule(LocalTime.of(18, 30), LocalTime.of(19, 0), NAME.H1);
        Rule h21 = newOnOffRule(LocalTime.of(9, 0), LocalTime.of(9, 30), NAME.H2);
        Rule h22 = newOnOffRule(LocalTime.of(18, 30), LocalTime.of(19, 0), NAME.H2);

        Rule b1 = newOnOffRule(LocalTime.of(3, 30), LocalTime.of(4, 30), NAME.B);
        Rule b2 = newOnOffRule(LocalTime.of(5, 15), LocalTime.of(6, 0), NAME.B);
        Rule b3 = newOnOffRule(LocalTime.of(6, 45), LocalTime.of(7, 30), NAME.B);
        Rule b4 = newOnOffRule(LocalTime.of(15, 0), LocalTime.of(16, 45), NAME.B);

        return new RuleSet(Stream.of(vp11, vp12, vp21, vp22, c1, c2, h11, h12, h21, h22, b1, b2, b3, b4));
    }

    private static Rule newOnRule(LocalTime start, NAME component) {
        return new Rule(new TimeTrigger(start), new SetActivityEffect(component, start));
    }

    private static Rule newOnOffRule(LocalTime start, LocalTime end, NAME component) {
        return new Rule(new TimeTrigger(start, Optional.of(end)), new SetActivityEffect(component, start));
    }

}
