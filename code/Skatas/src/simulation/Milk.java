package simulation;

public enum Milk {

    ;

    private static final double TIME_FOR_COOLING = 330; // in min
    private static final double MILK_PER_PROCESS = 2500; // in l

    public static final double WARM_MILK_TEMP = 35; // in °
    public static final double PRECOOLED_MILK_TEMP = 17; // in °
    public static final double COOLED_MILK_TEMP = 4; // in °

    public static final double MASS_MILK = 1.02; // in kg/l
    private static final double DENSITY_MILK = 3.85; // in kJ/(kg * K)

    public static final double MILK_FLOW_PER_MINUTE = 8.3;

    private static final double ENERGY_PER_PROCESS = energyNeccessary(MILK_PER_PROCESS, WARM_MILK_TEMP - COOLED_MILK_TEMP); // in
                                                                                                                            // kJ
    private static final double COOLING_POWER = ENERGY_PER_PROCESS / TIME_FOR_COOLING; // in kJ/min

    public static double energyNeccessary(double liters, double dTemp) {
        return (liters * MASS_MILK) * DENSITY_MILK * dTemp;
    }

    public static double incomingMilkTemp(boolean preCooling) {
        return preCooling ? PRECOOLED_MILK_TEMP : WARM_MILK_TEMP;
    }

    public static double mergeTemps(double oldMilk, double oldMilkTemp, double incomingMilk, double incomingMilkTemp) {
        return (oldMilk * oldMilkTemp + incomingMilk * incomingMilkTemp) / (oldMilk + incomingMilk);
    }

    public static double tempAfterCooling(double milk, double milkTemp, double minutes) {
        double energyNeccessary = energyNeccessary(milk, milkTemp - COOLED_MILK_TEMP);
        double energyAfterCooling = energyNeccessary - COOLING_POWER * minutes;
        double tempAfterCooling = energyAfterCooling < 0 ? COOLED_MILK_TEMP : milkTemp * energyAfterCooling / energyNeccessary;
        return tempAfterCooling < COOLED_MILK_TEMP ? COOLED_MILK_TEMP : tempAfterCooling;
    }

}
