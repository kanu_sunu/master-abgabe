package main;

import adaption.Adaption;
import adaption.RuleSet;
import evolution.Generation;
import evolution.Mutation;
import evolution.Recombination;
import evolution.Selection;

public class Enums {

    public enum G {
        LG {
            @Override
            public Generation get(int generationSize, Adaption adaption, RuleSet first, int ruleAmount, long seed) {
                return Generation.emptyGeneration(generationSize, adaption);
            }
        },
        IG {
            @Override
            public Generation get(int generationSize, Adaption adaption, RuleSet first, int ruleAmount, long seed) {
                return Generation.identicalGeneration(generationSize, adaption, first);
            }
        },
        ZG {
            @Override
            public Generation get(int generationSize, Adaption adaption, RuleSet first, int ruleAmount, long seed) {
                return Generation.randomGeneration(generationSize, ruleAmount, adaption, seed);
            }
        };

        public abstract Generation get(int generationSize, Adaption adaption, RuleSet first, int ruleAmount, long seed);
    }

    public enum M {
        KM {
            @Override
            public Mutation get() {
                return Mutation.noMutation();
            }
        },
        DM {
            @Override
            public Mutation get() {
                return Mutation.decrementalMutation();
            }
        },
        VM {
            @Override
            public Mutation get() {
                return Mutation.sharedMutation();
            }
        },
        ZM {
            @Override
            public Mutation get() {
                return Mutation.randomMutation();
            }
        };

        public abstract Mutation get();
    }

    public enum R {
        KR {
            @Override
            public Recombination get() {
                return Recombination.noRecombination();
            }
        },
        AR {
            @Override
            public Recombination get() {
                return Recombination.addingRecombination();
            }
        },
        TR {
            @Override
            public Recombination get() {
                return Recombination.splitRecombination();
            }
        },
        ZR {
            @Override
            public Recombination get() {
                return Recombination.randomRecombination();
            }
        };

        public abstract Recombination get();
    }

    public enum S {
        KS {
            @Override
            public Selection get() {
                return Selection.noSelection();
            }
        },
        BS {
            @Override
            public Selection get() {
                return Selection.onlyBestSelection();
            }
        },
        FS {
            @Override
            public Selection get() {
                return Selection.fitnessSelection();
            }
        },
        HS {
            @Override
            public Selection get() {
                return Selection.hybridSelection();
            }
        };

        public abstract Selection get();
    }

}
