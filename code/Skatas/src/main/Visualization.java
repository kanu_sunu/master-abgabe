package main;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.ApplicationFrame;

import adaption.Adaption;
import simulation.Progression;
import simulation.Simulation;

public enum Visualization {

    ;

    private static final String CHART_TITLE = "Tagesverlauf";
    private static final String FRAME_TITLE = "Frame";
    private static final String X_LABEL = "Uhrzeit";
    private static final String Y_LABEL = "Gesamtstrombedarf in W";

    private static final String LABEL_DEFAULT = "Standard-Ablauf";
    private static final String LABEL_BEST = "Bestes Individuum";
    private static final String LABEL_PV = "PV-Anlage";
    private static final String LABEL_ENERGY = "Energie";

    private static final String LABEL_LOWER = "Untere Grenze";
    private static final String LABEL_UPPER = "Obere Grenze";

    public static void visualize(Progression best, Adaption adaption) {
        JFreeChart chart = ChartFactory.createTimeSeriesChart(CHART_TITLE, X_LABEL, Y_LABEL, getData(best, adaption));
        ChartPanel panel = new ChartPanel(chart);
        ApplicationFrame frame = new ApplicationFrame(FRAME_TITLE);
        frame.setContentPane(panel);
        frame.pack();
        frame.setVisible(true);
    }

    private static XYDataset getData(Progression best, Adaption adaption) {
        TimeSeriesCollection result = new TimeSeriesCollection();
        Progression defaultP = adaption.getDefaultProgression();
        addValues(result, LABEL_DEFAULT, defaultP.getValues());
        addValues(result, LABEL_BEST, best.getValues());
        addValues(result, LABEL_PV, defaultP.getPvValues().map(v -> -v));
        addValues(result, LABEL_LOWER, Stream.generate(() -> adaption.getLowerThreshold()).limit(Simulation.SITUATIONS_PER_DAY));
        addValues(result, LABEL_UPPER, Stream.generate(() -> adaption.getUpperThreshold()).limit(Simulation.SITUATIONS_PER_DAY));
        addValues(result, LABEL_ENERGY, best.getEnergy());
        // Simulation.STANDARD_COMPONENTS
        // .forEach(c -> addValues(result, c.getName().toString(), defaultP.getConsumptionsFor(c.getName())));
        return result;
    }

    private static TimeSeriesCollection addValues(TimeSeriesCollection dataset, String category, Stream<Double> values) {
        int time = 0;
        TimeSeries series = new TimeSeries(category);
        for (double value : values.collect(Collectors.toList())) {
            series.add(new Millisecond(0, 0, time % 60, time / 60, 1, 1, 1900), value);
            time += 15;
        }
        dataset.addSeries(series);
        return dataset;
    }

}
