package main;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import adaption.Adaption;
import adaption.Rule;
import adaption.RuleSet;
import adaption.effect.DischargeEffect;
import adaption.effect.LoadingEffect;
import adaption.trigger.TimeTrigger;
import evolution.Generation;
import evolution.Individual;
import evolution.Mutation;
import evolution.Recombination;
import evolution.Selection;
import main.Enums.G;
import main.Enums.M;
import main.Enums.R;
import main.Enums.S;
import simulation.Component.NAME;
import simulation.Simulation;
import util.Lists;

public class Main {

    private static final long SEED = new Random().nextLong();

    private static final LocalDate SUMMER = LocalDate.of(2015, 7, 1);
    private static final LocalDate WINTER = LocalDate.of(2015, 1, 1);
    private static final RuleSet FIRST = intuitiveFirst();

    private static final int RULE_AMOUNT = 5;
    private static final int GENERATION_SIZE = 100;
    private static final int GENERATION_AMOUNT = 10;

    private static final Random _R = new Random(SEED);
    private static final int RUNS = 50;

    public static void main(String[] args) {

        System.err.println("SEED: " + SEED);
        System.err.println();

        RuleSet.PROB_NEWRULE = 0.75;
        RuleSet.PROB_REMOVAL = 0.25;
        Rule.ALLOW_THRESHOLDS = true;

        show(FIRST, SUMMER);
        evolution(FIRST, SUMMER, M.VM, S.FS, R.KR, G.IG);

        // compareDates(FIRST, SUMMER, WINTER, M.ZM, S.FS, R.TR, G.IG);

        // measure(FIRST, SUMMER, G.IG);
    }

    private static RuleSet intuitiveFirst() {
        Rule energyOn = new Rule(new TimeTrigger(LocalTime.of(12, 0)), new LoadingEffect(NAME.ES, 1.0));
        Rule energyOff = new Rule(new TimeTrigger(LocalTime.of(18, 30)), new DischargeEffect(NAME.ES, 0.8));

        Rule coolingOn1 = new Rule(new TimeTrigger(LocalTime.of(0, 0)), new LoadingEffect(NAME.CS, 1.0));
        Rule coolingOff1 = new Rule(new TimeTrigger(LocalTime.of(5, 45)), new DischargeEffect(NAME.CS, 0.0));
        Rule coolingOff2 = new Rule(new TimeTrigger(LocalTime.of(15, 45)), new DischargeEffect(NAME.CS, 0.0));

        return new RuleSet(Stream.of(energyOn, energyOff, coolingOn1, coolingOff1, coolingOff2));
    }

    private static RuleSet ownFirst() {
        Rule energyOn = new Rule(new TimeTrigger(LocalTime.of(12, 0)), new LoadingEffect(NAME.ES, 1.0));
        Rule energyOff = new Rule(new TimeTrigger(LocalTime.of(18, 30)), new DischargeEffect(NAME.ES, 0.8));

        Rule coolingOn1 = new Rule(new TimeTrigger(LocalTime.of(0, 0)), new LoadingEffect(NAME.CS, 1.0));
        Rule coolingOff1 = new Rule(new TimeTrigger(LocalTime.of(5, 45)), new DischargeEffect(NAME.CS, 0.0));
        Rule coolingOff2 = new Rule(new TimeTrigger(LocalTime.of(15, 45)), new DischargeEffect(NAME.CS, 0.0));

        return new RuleSet(Stream.of(energyOn, energyOff, coolingOn1, coolingOff1, coolingOff2));
    }

    public static void evolution(RuleSet first, LocalDate date, M m, S s, R r, G g) {
        Adaption adaption = new Adaption(date);
        Individual best = Adaption.adapt(GENERATION_SIZE, GENERATION_AMOUNT, s.get(), r.get(),
                g.get(GENERATION_SIZE, adaption, first, RULE_AMOUNT, _R.nextLong()), m.get(), _R.nextLong());
        System.out.println("Fitness:  " + best.getFitness() + "\n");
        Visualization.visualize(Simulation.simulate(date, best.getRuleSet(), Stream.empty()), adaption);
        System.out.println(best.simplify().getRuleSet());
    }

    public static void show(RuleSet first, LocalDate date) {
        Adaption adaption = new Adaption(date);
        Visualization.visualize(Simulation.simulate(date, first, Stream.empty()), adaption);
        System.out.println("Standard: " + adaption.getFitness(first));
    }

    public static void compareDates(RuleSet first, LocalDate firstDay, LocalDate secondDay, M m, S s, R r, G g) {
        Mutation _mutation = m.get();
        Selection _selection = s.get();
        Recombination _recombination = r.get();

        Adaption firstAdaption = new Adaption(firstDay);
        Adaption secondAdaption = new Adaption(secondDay);
        Generation _initial = g.get(GENERATION_SIZE, firstAdaption, first, RULE_AMOUNT, _R.nextLong());

        Individual firstResult = Adaption.adapt(GENERATION_SIZE, GENERATION_AMOUNT, _selection, _recombination, _initial, _mutation,
                _R.nextLong());
        Visualization.visualize(Simulation.simulate(firstDay, firstResult.getRuleSet(), Stream.empty()), firstAdaption);
        System.out.println("old: " + firstAdaption.getFitness(firstResult.getRuleSet()));
        System.out.println();
        System.out.println(firstResult.simplify());

        Individual secondResult = Adaption.adapt(GENERATION_SIZE, GENERATION_AMOUNT, _selection, _recombination,
                G.IG.get(GENERATION_SIZE, secondAdaption, firstResult.getRuleSet(), RULE_AMOUNT, _R.nextLong()), _mutation, _R.nextLong());
        Visualization.visualize(Simulation.simulate(secondDay, secondResult.getRuleSet(), Stream.empty()), secondAdaption);
        System.out.println("old: " + secondAdaption.getFitness(firstResult.getRuleSet()));
        System.out.println("new: " + secondAdaption.getFitness(secondResult.getRuleSet()));
        System.out.println();
        System.out.println(secondResult.simplify());
    }

    public static Double measure(RuleSet first, LocalDate date, G generation) {
        System.gc();
        Adaption adaption = new Adaption(date);
        List<Long> times = Lists.empty();
        for (M mutation : M.values()) {
            for (S selection : S.values()) {
                for (R recombination : R.values()) {
                    List<Individual> results = Lists.empty();

                    _R.longs().limit(RUNS).forEach(seed -> {
                        long start = System.currentTimeMillis();
                        results.add(Adaption.adapt(GENERATION_SIZE, GENERATION_AMOUNT, selection.get(), recombination.get(),
                                generation.get(GENERATION_SIZE, adaption, first, RULE_AMOUNT, _R.nextLong()), mutation.get(), seed));
                        times.add(System.currentTimeMillis() - start);
                    });

                    Individual best = results.stream().collect(Collectors.maxBy(Individual::compareTo)).get();
                    Individual worst = results.stream().collect(Collectors.minBy(Individual::compareTo)).get();
                    Double avgF = results.stream().collect(Collectors.averagingDouble(v -> v.getFitness()));

                    System.out.println("add(G." + generation.toString() + ", M." + mutation.toString() + ", R." + recombination.toString()
                            + ", S." + selection.toString() + ", " + scale(best.getFitness()) + ", " + scale(avgF) + ", "
                            + scale(worst.getFitness()) + ");");
                }
            }
        }
        return times.parallelStream().collect(Collectors.averagingDouble(v -> v));
    }

    private static final double scale(double value) {
        return Math.round(value * 100.0) / 100.0;
    }

}
