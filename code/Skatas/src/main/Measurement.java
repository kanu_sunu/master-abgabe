package main;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import main.Enums.G;
import main.Enums.M;
import main.Enums.R;
import main.Enums.S;
import tuple.T2;
import tuple.T3;
import tuple.T4;
import util.Lists;

public enum Measurement {

    ;

    private static final Map<T4<G, M, R, S>, T3<Double, Double, Double>> RESULTS = new HashMap<>();

    private static void add(G g, M m, R r, S s, double b, double a, double w) {
        T3<Double, Double, Double> old = RESULTS.put(new T4<>(g, m, r, s), new T3<>(b, a, w));
        if (old != null) {
            throw new IllegalArgumentException(new T4<>(g, m, r, s) + " already contained in Map");
        }
    }

    public static T4<G, M, R, S> getBest() {
        List<T2<T4<G, M, R, S>, Double>> results = Lists.empty();
        for (Entry<T4<G, M, R, S>, T3<Double, Double, Double>> entry : RESULTS.entrySet()) {
            T3<Double, Double, Double> values = entry.getValue();
            results.add(new T2<>(entry.getKey(), values.getA() + values.getB() + values.getC()));
        }
        return results.stream().collect(Collectors.maxBy((p1, p2) -> Double.compare(p1.getB(), p2.getB()))).get().getA();
    }

    public static String toCsv(Enums.G g) {
        StringBuilder result = new StringBuilder();
        result.append("conf,kr1,kr2,kr3,ar1,ar2,ar3,tr1,tr2,tr3,zr1,zr2,zr3\n");
        for (S s : S.values()) {
            for (R r : R.values()) {
                result.append(s).append(' ').append(r);
                for (M m : M.values()) {
                    T3<Double, Double, Double> entry = RESULTS.get(new T4<>(g, m, r, s));
                    result.append(',').append(format(entry.getA())).append(',').append(format(entry.getB())).append(',')
                            .append(format(entry.getC()));
                }
                result.append('\n');
            }
        }
        return result.toString();
    }

    private static String format(Double value) {
        String result = String.valueOf(value);
        return result.length() < 5 ? result + "0" : result;
    }

    public static void fillJanuary() {
        // ---------- 01.01.15 ---------- 100 Individuals

        // LG KM
        add(G.LG, M.KM, R.KR, S.KS, 20.67, 17.54, 14.43);
        add(G.LG, M.KM, R.AR, S.KS, 20.62, 17.31, 14.59);
        add(G.LG, M.KM, R.TR, S.KS, 20.74, 18.23, 14.88);
        add(G.LG, M.KM, R.ZR, S.KS, 20.29, 16.32, 14.54);

        add(G.LG, M.KM, R.KR, S.BS, 20.67, 16.47, 13.61);
        add(G.LG, M.KM, R.AR, S.BS, 20.29, 16.19, 12.92);
        add(G.LG, M.KM, R.TR, S.BS, 20.67, 16.5, 14.08);
        add(G.LG, M.KM, R.ZR, S.BS, 20.67, 16.76, 13.49);

        add(G.LG, M.KM, R.KR, S.FS, 20.34, 15.66, 13.15);
        add(G.LG, M.KM, R.AR, S.FS, 17.39, 14.92, 10.6);
        add(G.LG, M.KM, R.TR, S.FS, 21.11, 16.01, 13.43);
        add(G.LG, M.KM, R.ZR, S.FS, 20.67, 16.14, 13.5);

        add(G.LG, M.KM, R.KR, S.HS, 20.29, 15.73, 13.79);
        add(G.LG, M.KM, R.AR, S.HS, 20.62, 15.94, 12.97);
        add(G.LG, M.KM, R.TR, S.HS, 20.67, 16.89, 14.62);
        add(G.LG, M.KM, R.ZR, S.HS, 20.67, 16.0, 13.04);

        add(G.LG, M.DM, R.KR, S.KS, 21.68, 18.9, 16.48);
        add(G.LG, M.DM, R.AR, S.KS, 21.91, 19.0, 16.17);
        add(G.LG, M.DM, R.TR, S.KS, 21.45, 18.86, 16.03);
        add(G.LG, M.DM, R.ZR, S.KS, 21.23, 17.24, 14.59);

        add(G.LG, M.DM, R.KR, S.BS, 20.4, 17.41, 14.96);
        add(G.LG, M.DM, R.AR, S.BS, 20.67, 16.62, 14.89);
        add(G.LG, M.DM, R.TR, S.BS, 21.34, 17.74, 14.85);
        add(G.LG, M.DM, R.ZR, S.BS, 21.08, 17.07, 14.53);

        add(G.LG, M.DM, R.KR, S.FS, 20.93, 17.59, 14.75);
        add(G.LG, M.DM, R.AR, S.FS, 20.28, 16.75, 14.58);
        add(G.LG, M.DM, R.TR, S.FS, 21.07, 17.67, 15.35);
        add(G.LG, M.DM, R.ZR, S.FS, 20.42, 17.24, 14.75);

        add(G.LG, M.DM, R.KR, S.HS, 20.64, 17.54, 15.5);
        add(G.LG, M.DM, R.AR, S.HS, 20.4, 17.34, 15.05);
        add(G.LG, M.DM, R.TR, S.HS, 20.57, 17.89, 15.53);
        add(G.LG, M.DM, R.ZR, S.HS, 19.91, 16.82, 14.01);

        add(G.LG, M.VM, R.KR, S.KS, 22.12, 18.69, 15.53);
        add(G.LG, M.VM, R.AR, S.KS, 21.45, 19.34, 16.64);
        add(G.LG, M.VM, R.TR, S.KS, 20.82, 18.77, 15.46);
        add(G.LG, M.VM, R.ZR, S.KS, 20.57, 16.97, 13.9);

        add(G.LG, M.VM, R.KR, S.BS, 21.2, 18.03, 15.77);
        add(G.LG, M.VM, R.AR, S.BS, 20.74, 16.87, 13.97);
        add(G.LG, M.VM, R.TR, S.BS, 20.93, 17.99, 15.75);
        add(G.LG, M.VM, R.ZR, S.BS, 21.33, 17.3, 15.17);

        add(G.LG, M.VM, R.KR, S.FS, 20.69, 18.45, 16.03);
        add(G.LG, M.VM, R.AR, S.FS, 20.57, 17.26, 14.75);
        add(G.LG, M.VM, R.TR, S.FS, 20.57, 18.5, 16.03);
        add(G.LG, M.VM, R.ZR, S.FS, 20.57, 18.18, 15.46);

        add(G.LG, M.VM, R.KR, S.HS, 21.86, 18.51, 15.11);
        add(G.LG, M.VM, R.AR, S.HS, 20.96, 17.58, 14.75);
        add(G.LG, M.VM, R.TR, S.HS, 21.28, 18.4, 15.75);
        add(G.LG, M.VM, R.ZR, S.HS, 20.95, 18.08, 14.96);

        add(G.LG, M.ZM, R.KR, S.KS, 21.46, 19.06, 16.17);
        add(G.LG, M.ZM, R.AR, S.KS, 21.41, 18.95, 15.75);
        add(G.LG, M.ZM, R.TR, S.KS, 21.35, 18.87, 16.67);
        add(G.LG, M.ZM, R.ZR, S.KS, 20.8, 17.68, 14.36);

        add(G.LG, M.ZM, R.KR, S.BS, 20.57, 17.36, 15.31);
        add(G.LG, M.ZM, R.AR, S.BS, 20.57, 16.65, 14.33);
        add(G.LG, M.ZM, R.TR, S.BS, 20.55, 17.37, 15.11);
        add(G.LG, M.ZM, R.ZR, S.BS, 21.21, 17.48, 15.01);

        add(G.LG, M.ZM, R.KR, S.FS, 20.67, 18.25, 15.45);
        add(G.LG, M.ZM, R.AR, S.FS, 19.24, 16.9, 15.05);
        add(G.LG, M.ZM, R.TR, S.FS, 20.57, 18.17, 15.27);
        add(G.LG, M.ZM, R.ZR, S.FS, 20.57, 17.79, 16.03);

        add(G.LG, M.ZM, R.KR, S.HS, 21.06, 18.35, 16.04);
        add(G.LG, M.ZM, R.AR, S.HS, 20.57, 17.07, 15.11);
        add(G.LG, M.ZM, R.TR, S.HS, 20.57, 18.26, 16.03);
        add(G.LG, M.ZM, R.ZR, S.HS, 20.95, 17.87, 15.11);

        // IG KM
        add(G.IG, M.KM, R.KR, S.KS, 22.63, 18.13, 16.78);
        add(G.IG, M.KM, R.AR, S.KS, 22.63, 18.06, 16.12);
        add(G.IG, M.KM, R.TR, S.KS, 22.63, 18.12, 16.12);
        add(G.IG, M.KM, R.ZR, S.KS, 22.74, 18.07, 16.12);

        add(G.IG, M.KM, R.KR, S.BS, 22.74, 17.98, 16.12);
        add(G.IG, M.KM, R.AR, S.BS, 22.74, 17.88, 15.72);
        add(G.IG, M.KM, R.TR, S.BS, 22.74, 17.83, 15.72);
        add(G.IG, M.KM, R.ZR, S.BS, 22.74, 17.73, 15.11);

        add(G.IG, M.KM, R.KR, S.FS, 22.74, 17.82, 15.11);
        add(G.IG, M.KM, R.AR, S.FS, 22.74, 17.86, 15.11);
        add(G.IG, M.KM, R.TR, S.FS, 22.74, 17.90, 15.11);
        add(G.IG, M.KM, R.ZR, S.FS, 22.74, 17.96, 15.11);

        add(G.IG, M.KM, R.KR, S.HS, 22.74, 17.94, 15.11);
        add(G.IG, M.KM, R.AR, S.HS, 22.74, 17.92, 15.11);
        add(G.IG, M.KM, R.TR, S.HS, 22.74, 17.90, 15.11);
        add(G.IG, M.KM, R.ZR, S.HS, 22.74, 17.87, 15.11);

        // IG DM
        add(G.IG, M.DM, R.KR, S.KS, 22.74, 17.86, 15.11);
        add(G.IG, M.DM, R.AR, S.KS, 22.74, 17.74, 13.77);
        add(G.IG, M.DM, R.TR, S.KS, 22.74, 17.74, 13.77);
        add(G.IG, M.DM, R.ZR, S.KS, 22.74, 17.73, 13.77);

        add(G.IG, M.DM, R.KR, S.BS, 22.74, 17.66, 13.77);
        add(G.IG, M.DM, R.AR, S.BS, 22.74, 17.55, 13.18);
        add(G.IG, M.DM, R.TR, S.BS, 22.74, 17.50, 13.18);
        add(G.IG, M.DM, R.ZR, S.BS, 22.74, 17.44, 13.18);

        add(G.IG, M.DM, R.KR, S.FS, 22.74, 17.42, 13.18);
        add(G.IG, M.DM, R.AR, S.FS, 22.74, 17.34, 13.18);
        add(G.IG, M.DM, R.TR, S.FS, 22.74, 17.33, 13.18);
        add(G.IG, M.DM, R.ZR, S.FS, 22.74, 17.31, 13.18);

        add(G.IG, M.DM, R.KR, S.HS, 22.74, 17.29, 13.18);
        add(G.IG, M.DM, R.AR, S.HS, 22.74, 17.24, 13.18);
        add(G.IG, M.DM, R.TR, S.HS, 22.74, 17.23, 13.18);
        add(G.IG, M.DM, R.ZR, S.HS, 22.74, 17.21, 13.18);

        // IG VM
        add(G.IG, M.VM, R.KR, S.KS, 22.74, 17.23, 13.18);
        add(G.IG, M.VM, R.AR, S.KS, 22.74, 17.22, 13.18);
        add(G.IG, M.VM, R.TR, S.KS, 22.74, 17.26, 13.18);
        add(G.IG, M.VM, R.ZR, S.KS, 22.74, 17.26, 13.18);

        add(G.IG, M.VM, R.KR, S.BS, 22.74, 17.27, 13.18);
        add(G.IG, M.VM, R.AR, S.BS, 22.74, 17.23, 13.18);
        add(G.IG, M.VM, R.TR, S.BS, 22.74, 17.23, 13.18);
        add(G.IG, M.VM, R.ZR, S.BS, 22.74, 17.23, 13.18);

        add(G.IG, M.VM, R.KR, S.FS, 22.74, 17.25, 13.18);
        add(G.IG, M.VM, R.AR, S.FS, 22.74, 17.23, 13.18);
        add(G.IG, M.VM, R.TR, S.FS, 22.74, 17.24, 13.18);
        add(G.IG, M.VM, R.ZR, S.FS, 22.74, 17.25, 13.18);

        add(G.IG, M.VM, R.KR, S.HS, 22.74, 17.26, 13.18);
        add(G.IG, M.VM, R.AR, S.HS, 22.74, 17.25, 13.18);
        add(G.IG, M.VM, R.TR, S.HS, 22.74, 17.25, 13.18);
        add(G.IG, M.VM, R.ZR, S.HS, 22.74, 17.26, 13.18);

        // IG ZM
        add(G.IG, M.ZM, R.KR, S.KS, 22.74, 17.27, 13.18);
        add(G.IG, M.ZM, R.AR, S.KS, 22.74, 17.26, 13.18);
        add(G.IG, M.ZM, R.TR, S.KS, 22.74, 17.27, 13.18);
        add(G.IG, M.ZM, R.ZR, S.KS, 22.74, 17.28, 13.18);

        add(G.IG, M.ZM, R.KR, S.BS, 22.74, 17.27, 13.18);
        add(G.IG, M.ZM, R.AR, S.BS, 22.74, 17.26, 13.18);
        add(G.IG, M.ZM, R.TR, S.BS, 22.74, 17.26, 13.18);
        add(G.IG, M.ZM, R.ZR, S.BS, 22.74, 17.25, 13.18);

        add(G.IG, M.ZM, R.KR, S.FS, 22.74, 17.25, 13.18);
        add(G.IG, M.ZM, R.AR, S.FS, 22.74, 17.23, 13.18);
        add(G.IG, M.ZM, R.TR, S.FS, 22.74, 17.23, 13.18);
        add(G.IG, M.ZM, R.ZR, S.FS, 22.74, 17.23, 13.18);

        add(G.IG, M.ZM, R.KR, S.HS, 22.74, 17.24, 13.18);
        add(G.IG, M.ZM, R.AR, S.HS, 22.74, 17.22, 13.18);
        add(G.IG, M.ZM, R.TR, S.HS, 22.74, 17.23, 13.18);
        add(G.IG, M.ZM, R.ZR, S.HS, 22.74, 17.24, 13.18);

        // ZG KM
        add(G.ZG, M.KM, R.KR, S.KS, 20.67, 17.37, 14.03);
        add(G.ZG, M.KM, R.AR, S.KS, 20.67, 17.21, 14.03);
        add(G.ZG, M.KM, R.TR, S.KS, 21.71, 17.62, 14.03);
        add(G.ZG, M.KM, R.ZR, S.KS, 21.71, 17.54, 14.03);

        add(G.ZG, M.KM, R.KR, S.BS, 21.71, 17.34, 14.02);
        add(G.ZG, M.KM, R.AR, S.BS, 21.71, 17.20, 13.56);
        add(G.ZG, M.KM, R.TR, S.BS, 21.71, 17.10, 13.56);
        add(G.ZG, M.KM, R.ZR, S.BS, 21.71, 17.07, 13.56);

        add(G.ZG, M.KM, R.KR, S.FS, 21.71, 16.96, 13.33);
        add(G.ZG, M.KM, R.AR, S.FS, 21.71, 16.83, 12.26);
        add(G.ZG, M.KM, R.TR, S.FS, 21.71, 16.77, 12.26);
        add(G.ZG, M.KM, R.ZR, S.FS, 21.71, 16.72, 12.26);

        add(G.ZG, M.KM, R.KR, S.HS, 21.71, 16.68, 12.26);
        add(G.ZG, M.KM, R.AR, S.HS, 21.71, 16.64, 12.26);
        add(G.ZG, M.KM, R.TR, S.HS, 21.71, 16.62, 12.26);
        add(G.ZG, M.KM, R.ZR, S.HS, 21.71, 16.61, 12.26);

        // ZG DM
        add(G.ZG, M.DM, R.KR, S.KS, 21.71, 16.75, 12.26);
        add(G.ZG, M.DM, R.AR, S.KS, 21.72, 16.81, 12.26);
        add(G.ZG, M.DM, R.TR, S.KS, 22.63, 16.95, 12.26);
        add(G.ZG, M.DM, R.ZR, S.KS, 22.63, 17.05, 12.26);

        add(G.ZG, M.DM, R.KR, S.BS, 22.63, 17.07, 12.26);
        add(G.ZG, M.DM, R.AR, S.BS, 22.63, 17.08, 12.26);
        add(G.ZG, M.DM, R.TR, S.BS, 22.63, 17.11, 12.26);
        add(G.ZG, M.DM, R.ZR, S.BS, 22.63, 17.14, 12.26);

        add(G.ZG, M.DM, R.KR, S.FS, 22.63, 17.18, 12.26);
        add(G.ZG, M.DM, R.AR, S.FS, 22.63, 17.19, 12.26);
        add(G.ZG, M.DM, R.TR, S.FS, 22.63, 17.22, 12.26);
        add(G.ZG, M.DM, R.ZR, S.FS, 22.63, 17.24, 12.26);

        add(G.ZG, M.DM, R.KR, S.HS, 22.63, 17.27, 12.26);
        add(G.ZG, M.DM, R.AR, S.HS, 22.63, 17.26, 12.26);
        add(G.ZG, M.DM, R.TR, S.HS, 22.63, 17.29, 12.26);
        add(G.ZG, M.DM, R.ZR, S.HS, 22.63, 17.31, 12.26);

        // ZG VM
        add(G.ZG, M.VM, R.KR, S.KS, 22.63, 17.37, 12.26);
        add(G.ZG, M.VM, R.AR, S.KS, 22.63, 17.40, 12.26);
        add(G.ZG, M.VM, R.TR, S.KS, 22.63, 17.45, 12.26);
        add(G.ZG, M.VM, R.ZR, S.KS, 22.63, 17.48, 12.26);

        add(G.ZG, M.VM, R.KR, S.BS, 22.63, 17.48, 12.26);
        add(G.ZG, M.VM, R.AR, S.BS, 22.63, 17.47, 12.26);
        add(G.ZG, M.VM, R.TR, S.BS, 22.63, 17.49, 12.26);
        add(G.ZG, M.VM, R.ZR, S.BS, 22.63, 17.49, 12.26);

        add(G.ZG, M.VM, R.KR, S.FS, 22.63, 17.52, 12.26);
        add(G.ZG, M.VM, R.AR, S.FS, 22.63, 17.52, 12.26);
        add(G.ZG, M.VM, R.TR, S.FS, 22.63, 17.54, 12.26);
        add(G.ZG, M.VM, R.ZR, S.FS, 22.63, 17.56, 12.26);

        add(G.ZG, M.VM, R.KR, S.HS, 22.63, 17.58, 12.26);
        add(G.ZG, M.VM, R.AR, S.HS, 22.63, 17.57, 12.26);
        add(G.ZG, M.VM, R.TR, S.HS, 22.63, 17.60, 12.26);
        add(G.ZG, M.VM, R.ZR, S.HS, 22.63, 17.60, 12.26);

        // ZG ZM
        add(G.ZG, M.ZM, R.KR, S.KS, 22.63, 17.64, 12.26);
        add(G.ZG, M.ZM, R.AR, S.KS, 22.63, 17.63, 12.26);
        add(G.ZG, M.ZM, R.TR, S.KS, 22.63, 17.66, 12.26);
        add(G.ZG, M.ZM, R.ZR, S.KS, 22.63, 17.68, 12.26);

        add(G.ZG, M.ZM, R.KR, S.BS, 22.63, 17.67, 12.26);
        add(G.ZG, M.ZM, R.AR, S.BS, 22.63, 17.67, 12.26);
        add(G.ZG, M.ZM, R.TR, S.BS, 22.63, 17.67, 12.26);
        add(G.ZG, M.ZM, R.ZR, S.BS, 22.63, 17.66, 12.26);

        add(G.ZG, M.ZM, R.KR, S.FS, 22.63, 17.67, 12.26);
        add(G.ZG, M.ZM, R.AR, S.FS, 22.63, 17.67, 12.26);
        add(G.ZG, M.ZM, R.TR, S.FS, 22.63, 17.68, 12.26);
        add(G.ZG, M.ZM, R.ZR, S.FS, 22.63, 17.69, 12.26);

        add(G.ZG, M.ZM, R.KR, S.HS, 22.63, 17.70, 12.26);
        add(G.ZG, M.ZM, R.AR, S.HS, 22.63, 17.69, 12.26);
        add(G.ZG, M.ZM, R.TR, S.HS, 22.63, 17.70, 12.26);
        add(G.ZG, M.ZM, R.ZR, S.HS, 22.63, 17.71, 12.26);
    }

    public static void fillJuly() {
        // ---------- 01.07.15 ---------- 100 Individuals

        // LG
        add(G.LG, M.KM, R.KR, S.KS, 32.56, 27.97, 25.27);
        add(G.LG, M.KM, R.AR, S.KS, 32.34, 30.19, 27.31);
        add(G.LG, M.KM, R.TR, S.KS, 32.29, 28.55, 25.42);
        add(G.LG, M.KM, R.ZR, S.KS, 31.59, 26.41, 14.98);
        add(G.LG, M.KM, R.KR, S.BS, 31.62, 27.55, 24.08);
        add(G.LG, M.KM, R.AR, S.BS, 32.43, 28.36, 24.94);
        add(G.LG, M.KM, R.TR, S.BS, 32.56, 27.72, 24.12);
        add(G.LG, M.KM, R.ZR, S.BS, 30.81, 26.97, 23.78);
        add(G.LG, M.KM, R.KR, S.FS, 31.52, 27.52, 19.55);
        add(G.LG, M.KM, R.AR, S.FS, 31.35, 27.55, 24.57);
        add(G.LG, M.KM, R.TR, S.FS, 31.79, 27.39, 24.21);
        add(G.LG, M.KM, R.ZR, S.FS, 31.96, 26.73, 22.27);
        add(G.LG, M.KM, R.KR, S.HS, 30.82, 27.59, 23.32);
        add(G.LG, M.KM, R.AR, S.HS, 31.48, 28.54, 24.64);
        add(G.LG, M.KM, R.TR, S.HS, 31.48, 28.33, 25.05);
        add(G.LG, M.KM, R.ZR, S.HS, 32.39, 27.43, 24.89);
        add(G.LG, M.DM, R.KR, S.KS, 32.12, 29.27, 26.17);
        add(G.LG, M.DM, R.AR, S.KS, 33.76, 30.95, 28.92);
        add(G.LG, M.DM, R.TR, S.KS, 32.29, 29.1, 25.7);
        add(G.LG, M.DM, R.ZR, S.KS, 31.35, 27.05, 22.48);
        add(G.LG, M.DM, R.KR, S.BS, 32.22, 29.15, 25.91);
        add(G.LG, M.DM, R.AR, S.BS, 32.12, 29.76, 27.21);
        add(G.LG, M.DM, R.TR, S.BS, 32.29, 29.18, 26.05);
        add(G.LG, M.DM, R.ZR, S.BS, 32.29, 28.38, 24.72);
        add(G.LG, M.DM, R.KR, S.FS, 32.29, 29.13, 25.2);
        add(G.LG, M.DM, R.AR, S.FS, 31.69, 30.18, 27.6);
        add(G.LG, M.DM, R.TR, S.FS, 31.96, 29.35, 26.43);
        add(G.LG, M.DM, R.ZR, S.FS, 31.79, 29.26, 24.82);
        add(G.LG, M.DM, R.KR, S.HS, 31.96, 29.18, 26.0);
        add(G.LG, M.DM, R.AR, S.HS, 32.48, 30.12, 27.17);
        add(G.LG, M.DM, R.TR, S.HS, 32.19, 29.69, 27.06);
        add(G.LG, M.DM, R.ZR, S.HS, 31.35, 28.36, 24.97);
        add(G.LG, M.VM, R.KR, S.KS, 32.29, 28.89, 25.32);
        add(G.LG, M.VM, R.AR, S.KS, 32.76, 30.65, 28.54);
        add(G.LG, M.VM, R.TR, S.KS, 32.12, 28.83, 25.39);
        add(G.LG, M.VM, R.ZR, S.KS, 31.96, 26.21, 14.4);
        add(G.LG, M.VM, R.KR, S.BS, 32.29, 29.48, 25.93);
        add(G.LG, M.VM, R.AR, S.BS, 32.56, 29.94, 27.7);
        add(G.LG, M.VM, R.TR, S.BS, 32.29, 29.22, 25.49);
        add(G.LG, M.VM, R.ZR, S.BS, 31.86, 28.46, 23.53);
        add(G.LG, M.VM, R.KR, S.FS, 32.56, 29.79, 26.11);
        add(G.LG, M.VM, R.AR, S.FS, 32.56, 30.24, 27.81);
        add(G.LG, M.VM, R.TR, S.FS, 32.56, 29.65, 26.18);
        add(G.LG, M.VM, R.ZR, S.FS, 32.56, 29.06, 25.42);
        add(G.LG, M.VM, R.KR, S.HS, 32.29, 29.11, 25.93);
        add(G.LG, M.VM, R.AR, S.HS, 32.59, 30.24, 27.87);
        add(G.LG, M.VM, R.TR, S.HS, 31.96, 29.35, 25.49);
        add(G.LG, M.VM, R.ZR, S.HS, 32.56, 27.58, 23.95);
        add(G.LG, M.ZM, R.KR, S.KS, 32.29, 29.49, 25.93);
        add(G.LG, M.ZM, R.AR, S.KS, 32.79, 30.95, 28.81);
        add(G.LG, M.ZM, R.TR, S.KS, 32.33, 29.25, 25.59);
        add(G.LG, M.ZM, R.ZR, S.KS, 31.96, 27.07, 22.82);
        add(G.LG, M.ZM, R.KR, S.BS, 31.79, 29.17, 26.5);
        add(G.LG, M.ZM, R.AR, S.BS, 32.86, 29.87, 27.21);
        add(G.LG, M.ZM, R.TR, S.BS, 32.56, 29.47, 25.59);
        add(G.LG, M.ZM, R.ZR, S.BS, 31.84, 28.58, 25.83);
        add(G.LG, M.ZM, R.KR, S.FS, 32.12, 29.54, 26.46);
        add(G.LG, M.ZM, R.AR, S.FS, 32.19, 29.47, 27.12);
        add(G.LG, M.ZM, R.TR, S.FS, 32.39, 29.68, 27.04);
        add(G.LG, M.ZM, R.ZR, S.FS, 32.12, 29.61, 25.93);
        add(G.LG, M.ZM, R.KR, S.HS, 31.79, 29.53, 26.5);
        add(G.LG, M.ZM, R.AR, S.HS, 31.68, 29.9, 27.95);
        add(G.LG, M.ZM, R.TR, S.HS, 32.03, 29.73, 26.75);
        add(G.LG, M.ZM, R.ZR, S.HS, 31.96, 28.93, 25.59);

        // ZG
        add(G.ZG, M.KM, R.KR, S.KS, 32.56, 29.86, 26.28);
        add(G.ZG, M.KM, R.AR, S.KS, 33.29, 30.46, 27.47);
        add(G.ZG, M.KM, R.TR, S.KS, 32.29, 30.45, 27.93);
        add(G.ZG, M.KM, R.ZR, S.KS, 31.86, 29.41, 26.65);
        add(G.ZG, M.KM, R.KR, S.BS, 32.56, 29.19, 25.45);
        add(G.ZG, M.KM, R.AR, S.BS, 31.89, 29.69, 27.19);
        add(G.ZG, M.KM, R.TR, S.BS, 31.35, 29.1, 26.11);
        add(G.ZG, M.KM, R.ZR, S.BS, 32.12, 29.27, 26.53);
        add(G.ZG, M.KM, R.KR, S.FS, 32.05, 28.64, 25.83);
        add(G.ZG, M.KM, R.AR, S.FS, 32.25, 28.88, 25.66);
        add(G.ZG, M.KM, R.TR, S.FS, 32.12, 29.33, 26.16);
        add(G.ZG, M.KM, R.ZR, S.FS, 32.12, 29.52, 26.77);
        add(G.ZG, M.KM, R.KR, S.HS, 32.56, 29.01, 25.9);
        add(G.ZG, M.KM, R.AR, S.HS, 32.56, 29.43, 25.77);
        add(G.ZG, M.KM, R.TR, S.HS, 32.56, 29.75, 26.99);
        add(G.ZG, M.KM, R.ZR, S.HS, 34.72, 29.69, 26.6);
        add(G.ZG, M.DM, R.KR, S.KS, 33.39, 30.72, 29.12);
        add(G.ZG, M.DM, R.AR, S.KS, 32.88, 30.8, 28.95);
        add(G.ZG, M.DM, R.TR, S.KS, 33.79, 31.11, 29.53);
        add(G.ZG, M.DM, R.ZR, S.KS, 32.12, 30.46, 28.0);
        add(G.ZG, M.DM, R.KR, S.BS, 32.29, 30.52, 28.67);
        add(G.ZG, M.DM, R.AR, S.BS, 32.39, 30.51, 28.85);
        add(G.ZG, M.DM, R.TR, S.BS, 34.04, 30.57, 28.27);
        add(G.ZG, M.DM, R.ZR, S.BS, 32.83, 30.51, 27.64);
        add(G.ZG, M.DM, R.KR, S.FS, 33.76, 30.97, 28.96);
        add(G.ZG, M.DM, R.AR, S.FS, 32.41, 30.51, 28.93);
        add(G.ZG, M.DM, R.TR, S.FS, 32.12, 30.79, 28.51);
        add(G.ZG, M.DM, R.ZR, S.FS, 33.26, 30.68, 27.83);
        add(G.ZG, M.DM, R.KR, S.HS, 33.42, 30.67, 28.78);
        add(G.ZG, M.DM, R.AR, S.HS, 32.63, 30.41, 28.32);
        add(G.ZG, M.DM, R.TR, S.HS, 32.37, 30.62, 28.49);
        add(G.ZG, M.DM, R.ZR, S.HS, 32.56, 30.78, 28.41);
        add(G.ZG, M.VM, R.KR, S.KS, 32.56, 30.48, 27.95);
        add(G.ZG, M.VM, R.AR, S.KS, 32.66, 30.73, 28.94);
        add(G.ZG, M.VM, R.TR, S.KS, 32.06, 30.54, 27.77);
        add(G.ZG, M.VM, R.ZR, S.KS, 32.29, 30.0, 27.2);
        add(G.ZG, M.VM, R.KR, S.BS, 32.39, 30.65, 28.56);
        add(G.ZG, M.VM, R.AR, S.BS, 33.59, 30.65, 28.73);
        add(G.ZG, M.VM, R.TR, S.BS, 32.38, 30.11, 28.07);
        add(G.ZG, M.VM, R.ZR, S.BS, 32.56, 30.26, 28.24);
        add(G.ZG, M.VM, R.KR, S.FS, 32.29, 30.38, 26.72);
        add(G.ZG, M.VM, R.AR, S.FS, 31.96, 30.45, 28.19);
        add(G.ZG, M.VM, R.TR, S.FS, 32.39, 30.73, 28.87);
        add(G.ZG, M.VM, R.ZR, S.FS, 32.29, 30.6, 28.73);
        add(G.ZG, M.VM, R.KR, S.HS, 32.58, 30.45, 28.33);
        add(G.ZG, M.VM, R.AR, S.HS, 33.37, 30.45, 28.18);
        add(G.ZG, M.VM, R.TR, S.HS, 33.33, 30.73, 28.56);
        add(G.ZG, M.VM, R.ZR, S.HS, 33.16, 30.44, 28.87);
        add(G.ZG, M.ZM, R.KR, S.KS, 32.62, 31.02, 29.16);
        add(G.ZG, M.ZM, R.AR, S.KS, 33.95, 31.14, 29.14);
        add(G.ZG, M.ZM, R.TR, S.KS, 32.98, 31.24, 29.11);
        add(G.ZG, M.ZM, R.ZR, S.KS, 32.65, 30.41, 28.07);
        add(G.ZG, M.ZM, R.KR, S.BS, 33.21, 30.49, 27.66);
        add(G.ZG, M.ZM, R.AR, S.BS, 32.56, 30.2, 28.25);
        add(G.ZG, M.ZM, R.TR, S.BS, 31.97, 30.42, 28.24);
        add(G.ZG, M.ZM, R.ZR, S.BS, 32.43, 30.39, 27.81);
        add(G.ZG, M.ZM, R.KR, S.FS, 32.56, 30.82, 27.8);
        add(G.ZG, M.ZM, R.AR, S.FS, 32.38, 30.43, 28.51);
        add(G.ZG, M.ZM, R.TR, S.FS, 32.91, 30.79, 28.98);
        add(G.ZG, M.ZM, R.ZR, S.FS, 32.55, 30.89, 28.84);
        add(G.ZG, M.ZM, R.KR, S.HS, 33.02, 30.78, 29.04);
        add(G.ZG, M.ZM, R.AR, S.HS, 32.73, 30.49, 28.43);
        add(G.ZG, M.ZM, R.TR, S.HS, 32.12, 30.79, 28.86);
        add(G.ZG, M.ZM, R.ZR, S.HS, 32.52, 30.42, 28.6);

        // IG
        add(G.IG, M.KM, R.KR, S.KS, 33.96, 31.93, 30.24);
        add(G.IG, M.KM, R.AR, S.KS, 32.86, 31.4, 29.61);
        add(G.IG, M.KM, R.TR, S.KS, 33.6, 32.27, 30.99);
        add(G.IG, M.KM, R.ZR, S.KS, 33.52, 31.77, 30.25);
        add(G.IG, M.KM, R.KR, S.BS, 33.95, 31.97, 30.58);
        add(G.IG, M.KM, R.AR, S.BS, 32.95, 31.15, 29.54);
        add(G.IG, M.KM, R.TR, S.BS, 34.0, 31.93, 30.25);
        add(G.IG, M.KM, R.ZR, S.BS, 32.64, 31.06, 28.81);
        add(G.IG, M.KM, R.KR, S.FS, 34.44, 31.91, 30.15);
        add(G.IG, M.KM, R.AR, S.FS, 33.08, 31.54, 29.74);
        add(G.IG, M.KM, R.TR, S.FS, 33.64, 32.32, 30.9);
        add(G.IG, M.KM, R.ZR, S.FS, 33.99, 32.15, 30.76);
        add(G.IG, M.KM, R.KR, S.HS, 33.18, 31.92, 29.17);
        add(G.IG, M.KM, R.AR, S.HS, 33.1, 31.29, 29.58);
        add(G.IG, M.KM, R.TR, S.HS, 33.89, 32.06, 29.82);
        add(G.IG, M.KM, R.ZR, S.HS, 32.64, 31.26, 29.14);
        add(G.IG, M.DM, R.KR, S.KS, 32.58, 30.97, 29.41);
        add(G.IG, M.DM, R.AR, S.KS, 32.65, 30.48, 28.76);
        add(G.IG, M.DM, R.TR, S.KS, 32.66, 31.15, 29.11);
        add(G.IG, M.DM, R.ZR, S.KS, 32.56, 30.39, 27.35);
        add(G.IG, M.DM, R.KR, S.BS, 32.73, 30.14, 27.9);
        add(G.IG, M.DM, R.AR, S.BS, 32.92, 30.43, 28.43);
        add(G.IG, M.DM, R.TR, S.BS, 32.73, 30.51, 27.94);
        add(G.IG, M.DM, R.ZR, S.BS, 31.89, 30.2, 26.4);
        add(G.IG, M.DM, R.KR, S.FS, 33.06, 30.35, 27.48);
        add(G.IG, M.DM, R.AR, S.FS, 33.15, 30.48, 28.49);
        add(G.IG, M.DM, R.TR, S.FS, 32.12, 30.47, 27.83);
        add(G.IG, M.DM, R.ZR, S.FS, 32.64, 30.57, 28.34);
        add(G.IG, M.DM, R.KR, S.HS, 32.4, 30.34, 27.92);
        add(G.IG, M.DM, R.AR, S.HS, 32.97, 30.43, 28.04);
        add(G.IG, M.DM, R.TR, S.HS, 33.16, 30.73, 28.05);
        add(G.IG, M.DM, R.ZR, S.HS, 33.11, 30.38, 27.45);
        add(G.IG, M.VM, R.KR, S.KS, 32.12, 30.82, 29.05);
        add(G.IG, M.VM, R.AR, S.KS, 33.09, 30.82, 29.31);
        add(G.IG, M.VM, R.TR, S.KS, 33.18, 31.09, 28.14);
        add(G.IG, M.VM, R.ZR, S.KS, 32.56, 30.42, 27.33);
        add(G.IG, M.VM, R.KR, S.BS, 33.76, 30.9, 27.29);
        add(G.IG, M.VM, R.AR, S.BS, 32.89, 30.95, 29.37);
        add(G.IG, M.VM, R.TR, S.BS, 32.67, 30.96, 27.81);
        add(G.IG, M.VM, R.ZR, S.BS, 32.9, 31.06, 28.36);
        add(G.IG, M.VM, R.KR, S.FS, 33.74, 30.61, 26.47);
        add(G.IG, M.VM, R.AR, S.FS, 33.11, 30.77, 29.21);
        add(G.IG, M.VM, R.TR, S.FS, 32.29, 31.06, 29.29);
        add(G.IG, M.VM, R.ZR, S.FS, 32.39, 30.94, 28.69);
        add(G.IG, M.VM, R.KR, S.HS, 32.15, 30.62, 27.48);
        add(G.IG, M.VM, R.AR, S.HS, 33.28, 31.05, 29.31);
        add(G.IG, M.VM, R.TR, S.HS, 32.29, 30.72, 28.04);
        add(G.IG, M.VM, R.ZR, S.HS, 33.08, 31.06, 27.37);
        add(G.IG, M.ZM, R.KR, S.KS, 32.73, 31.32, 29.93);
        add(G.IG, M.ZM, R.AR, S.KS, 33.34, 31.48, 29.55);
        add(G.IG, M.ZM, R.TR, S.KS, 33.82, 31.27, 29.61);
        add(G.IG, M.ZM, R.ZR, S.KS, 33.76, 30.88, 28.6);
        add(G.IG, M.ZM, R.KR, S.BS, 34.45, 31.11, 28.96);
        add(G.IG, M.ZM, R.AR, S.BS, 33.55, 31.48, 29.71);
        add(G.IG, M.ZM, R.TR, S.BS, 32.12, 30.96, 29.23);
        add(G.IG, M.ZM, R.ZR, S.BS, 33.23, 30.64, 28.16);
        add(G.IG, M.ZM, R.KR, S.FS, 33.92, 31.81, 29.03);
        add(G.IG, M.ZM, R.AR, S.FS, 33.73, 31.57, 29.73);
        add(G.IG, M.ZM, R.TR, S.FS, 34.05, 31.9, 29.82);
        add(G.IG, M.ZM, R.ZR, S.FS, 33.55, 31.64, 29.57);
        add(G.IG, M.ZM, R.KR, S.HS, 33.33, 31.33, 29.3);
        add(G.IG, M.ZM, R.AR, S.HS, 33.32, 31.37, 29.64);
        add(G.IG, M.ZM, R.TR, S.HS, 34.03, 31.27, 29.03);
        add(G.IG, M.ZM, R.ZR, S.HS, 33.76, 30.89, 28.24);
    }

}
