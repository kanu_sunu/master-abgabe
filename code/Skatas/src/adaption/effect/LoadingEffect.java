package adaption.effect;

import java.util.Random;

import simulation.Component;
import simulation.Component.NAME;
import simulation.Situation;
import simulation.components.Storage;

public class LoadingEffect implements Effect {

    private static final double STEP_WIDTH = 0.1;

    private final double threshold;
    private final NAME name;

    public LoadingEffect(NAME name, double threshold) {
        this.name = name;
        this.threshold = threshold;
    }

    public LoadingEffect(long seed) {
        Random r = new Random(seed);
        this.name = Component.randomStorage(r.nextLong());
        this.threshold = r.nextInt(11) * STEP_WIDTH;
    }

    @Override
    public Component effect(Component component, Situation situation) {
        return ((Storage) component).startLoading(this.threshold);
    }

    @Override
    public Effect mutate(double intensity, long seed) {
        Random r = new Random(seed);
        double plus = r.nextDouble() * intensity - (intensity / 2);
        return new LoadingEffect(this.name, Math.max(0, Math.min(1, this.threshold + plus)));
    }

    @Override
    public String toString() {
        return "load " + this.name + " to " + this.threshold;
    }

    @Override
    public NAME getName() {
        return this.name;
    }
}
