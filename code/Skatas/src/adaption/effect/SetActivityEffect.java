package adaption.effect;

import java.time.LocalTime;

import simulation.Component;
import simulation.Component.NAME;
import simulation.Situation;

public class SetActivityEffect implements Effect {

    private final LocalTime start;
    private final NAME name;

    public SetActivityEffect(NAME name, LocalTime start) {
        this.name = name;
        this.start = start;
    }

    @Override
    public Component effect(Component component, Situation situation) {
        return component.setActivity(this.start.equals(situation.getMoment().toLocalTime()));
    }

    @Override
    public Effect mutate(double intensity, long seed) {
        return this;
    }

    @Override
    public String toString() {
        return this.name + ": SetActivity(" + this.start + ")";
    }

    @Override
    public NAME getName() {
        return this.name;
    }

}
