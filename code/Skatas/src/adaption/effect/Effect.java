package adaption.effect;

import simulation.Component;
import simulation.Component.NAME;
import simulation.Situation;

public interface Effect {

	public default Component apply(Component component, Situation situation) {
		return component.getName() == this.getName() ? this.effect(component, situation) : component;
	}

	public Component effect(Component component, Situation situation);

	public Effect mutate(double intensity, long seed);

	public NAME getName();

}
