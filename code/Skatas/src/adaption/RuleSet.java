package adaption;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

import evolution.Mutation;
import util.Lists;

public class RuleSet {

    public static double PROB_NEWRULE = 0.5;
    public static double PROB_REMOVAL = 0.0;

    public static final RuleSet EMPTY = new RuleSet(Stream.of());

    private final List<Rule> rules = new LinkedList<>();

    public RuleSet(Stream<Rule> rules) {
        rules.forEach(this.rules::add);
    }

    public static RuleSet random(int ruleAmount, long seed) {
        return new RuleSet(new Random(seed).longs().mapToObj(s -> Rule.random(s)).limit(ruleAmount));
    }

    public Stream<Rule> getRules() {
        return this.rules.stream();
    }

    public RuleSet addRules(RuleSet newRules) {
        return new RuleSet(Stream.concat(this.rules.stream(), newRules.getRules()));
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        this.rules.stream().map(Rule::toString).sorted((s1, s2) -> s1.compareTo(s2))
                .forEachOrdered(rule -> result.append(rule.toString()).append('\n'));
        return result.toString();
    }

    public RuleSet mutate(Mutation mutation, int currentGeneration, int generationAmount, long seed) {
        Random r = new Random(seed);
        List<Rule> newRules = Lists.of(this.rules);
        if (r.nextDouble() < PROB_REMOVAL && newRules.size() > 0) {
            newRules.remove(r.nextInt(newRules.size()));
        }
        if (r.nextDouble() < PROB_NEWRULE) {
            newRules.add(Rule.random(r.nextLong()));
        }
        Collections.shuffle(newRules, r);
        List<Rule> result = Lists.of();
        for (int i = 0; i < newRules.size(); i++) {
            double intensity = mutation.getIntensity(currentGeneration, generationAmount, i + 1, newRules.size(), r.nextLong());
            result.add(newRules.get(i).mutate(intensity, r.nextLong()));
        }
        return new RuleSet(result.stream());
    }

}
