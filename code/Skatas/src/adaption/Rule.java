package adaption;

import java.util.Random;

import adaption.effect.DischargeEffect;
import adaption.effect.Effect;
import adaption.effect.LoadingEffect;
import adaption.trigger.ThresholdTrigger;
import adaption.trigger.TimeTrigger;
import adaption.trigger.Trigger;
import simulation.Component;
import simulation.Situation;

public class Rule {

    private int activations = 0;
    private final Trigger trigger;
    private final Effect effect;

    public static boolean ALLOW_THRESHOLDS = true;

    public Rule(Trigger trigger, Effect effect) {
        this.trigger = trigger;
        this.effect = effect;
    }

    public static Rule random(long seed) {
        Random r = new Random(seed);
        Trigger trigger = ALLOW_THRESHOLDS ? (r.nextBoolean() ? new ThresholdTrigger(r.nextLong()) : new TimeTrigger(r.nextLong()))
                : new TimeTrigger(r.nextLong());
        Effect effect = r.nextBoolean() ? new LoadingEffect(r.nextLong()) : new DischargeEffect(r.nextLong());
        return new Rule(trigger, effect);
    }

    public final Component affect(Component component, Situation situation) {
        return this.trigger.triggers(component, situation) ? this.effect.apply(component, situation) : component;
    }

    public final boolean affects(Component component, Situation situation) {
        return this.effect.getName().equals(component.getName()) && this.trigger.triggers(component, situation);
    }

    public final Rule mutate(double intensity, long seed) {
        return new Rule(this.trigger.mutate(intensity, seed), this.effect.mutate(intensity, seed));
    }

    @Override
    public String toString() {
        return "If " + this.trigger.toString() + " then " + this.effect;
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && this.toString().equals(obj.toString());
    }

    @Override
    public int hashCode() {
        return this.toString().hashCode();
    }

    public final int getActivations() {
        return this.activations;
    }

    public final Trigger getTrigger() {
        return this.trigger;
    }

    public void use() {
        this.activations++;
    }

}
