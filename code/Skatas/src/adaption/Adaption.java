package adaption;

import java.time.LocalDate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import evolution.Evolution;
import evolution.Generation;
import evolution.Individual;
import evolution.Mutation;
import evolution.Recombination;
import evolution.Selection;
import simulation.Progression;
import simulation.Simulation;

public class Adaption {

    private static final double LOWER_THRESHOLD = 1.00;
    private static final double UPPER_THRESHOLD = 0.95;
    private static final double UPPER_FINE = 20;

    private final LocalDate date;
    private final double upperThreshold;
    private final double lowerThreshold;
    private final Progression defaultProgression;

    private double defaultFitness;

    public Adaption(LocalDate date) {
        this.date = date;
        this.defaultProgression = Simulation.simulate(date, RuleSet.EMPTY, Stream.of());
        this.upperThreshold = evaluateUpperThreshold(this.defaultProgression);
        this.lowerThreshold = evaluateLowerThreshold(this.defaultProgression);
        this.defaultFitness = evaluateFitness(this.defaultProgression, false);
    }

    public double getFitness(RuleSet current) {
        return evaluateFitness(Simulation.simulate(this.date, current, Stream.of()), true);
    }

    public double evaluateFitness(Progression progression, boolean scaling) {
        double fitness = progression.getValues().collect(Collectors.summingDouble(this::adjust));
        return scaling ? scale(fitness) : fitness;
    }

    private double adjust(double value) {
        if (value <= this.lowerThreshold) {
            return 0;
        } else if (value <= this.upperThreshold) {
            return value - this.lowerThreshold;
        } else {
            return UPPER_FINE * (value - this.upperThreshold) + (this.upperThreshold - this.lowerThreshold);
        }
    }

    private double scale(double fitness) {
        return (this.defaultFitness - fitness) / this.defaultFitness * 100;
    }

    private static double evaluateLowerThreshold(Progression progression) {
        return progression.getPvValues().max((a, b) -> (int) (a - b)).get() * LOWER_THRESHOLD;
    }

    private static double evaluateUpperThreshold(Progression progression) {
        return progression.getValues().max((a, b) -> (int) (a - b)).get() * UPPER_THRESHOLD;
    }

    public static Individual adapt(int generationSize, int generationAmount, Selection selection, Recombination recombination,
            Generation first, Mutation mutation, long seed) {
        Individual bestOfFirstGeneration = first.getBestIndividual();
        Individual result = Evolution.start(mutation, selection, recombination, first, generationSize, generationAmount, seed);
        return result.getFitness() > bestOfFirstGeneration.getFitness() ? result : bestOfFirstGeneration;
    }

    public double getUpperThreshold() {
        return this.upperThreshold;
    }

    public double getLowerThreshold() {
        return this.lowerThreshold;
    }

    public Progression getDefaultProgression() {
        return this.defaultProgression;
    }

}
