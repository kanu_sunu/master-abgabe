package adaption.trigger;

import simulation.Component;
import simulation.Situation;

public interface Trigger {

    public boolean triggers(Component component, Situation situation);

    public Trigger mutate(double intensity, long seed);

}
