package adaption.trigger;

import java.util.Random;

import simulation.Component;
import simulation.Situation;

public class ThresholdTrigger implements Trigger {

    public static final double MAX_THRESHOLD = 40000;

    private final double threshold;
    private final boolean bigger;

    public ThresholdTrigger(double threshold, boolean bigger) {
        this.threshold = threshold;
        this.bigger = bigger;
    }

    public ThresholdTrigger(long seed) {
        Random r = new Random(seed);
        this.threshold = r.nextDouble() * MAX_THRESHOLD;
        this.bigger = r.nextBoolean();
    }

    @Override
    public boolean triggers(Component component, Situation situation) {
        return this.bigger ? situation.getConsumption() >= this.threshold : situation.getConsumption() <= this.threshold;
    }

    @Override
    public Trigger mutate(double intensity, long seed) {
        Random r = new Random(seed);
        double plus = (r.nextDouble() * intensity - (intensity / 2)) * MAX_THRESHOLD;
        double newThreshold = Math.max(0, Math.min(this.threshold + plus, MAX_THRESHOLD));
        boolean newBigger = r.nextDouble() < intensity ? !this.bigger : this.bigger;
        return new ThresholdTrigger(newThreshold, newBigger);
    }

    @Override
    public String toString() {
        return "consumption " + (this.bigger ? ">=" : "<=") + " " + this.threshold;
    }

    public double getThreshold() {
        return this.threshold;
    }

}
