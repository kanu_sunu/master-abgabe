package adaption.trigger;

import java.time.LocalTime;
import java.util.Optional;
import java.util.Random;

import simulation.Component;
import simulation.Situation;

public class TimeTrigger implements Trigger {

    public static final int MAX_MUTATION = 40;

    private final LocalTime start;
    private final Optional<LocalTime> end;

    public TimeTrigger(int startHour, int startMinute) {
        this.start = LocalTime.of(startHour, startMinute);
        this.end = Optional.empty();
    }

    public TimeTrigger(long seed) {
        Random r = new Random(seed);
        this.start = LocalTime.of(r.nextInt(24), 15 * r.nextInt(4));
        this.end = Optional.empty();
    }

    public TimeTrigger(LocalTime start) {
        this.start = start;
        this.end = Optional.empty();
    }

    public TimeTrigger(LocalTime start, Optional<LocalTime> end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public boolean triggers(Component component, Situation situation) {
        LocalTime current = situation.getMoment().toLocalTime();
        return this.end.isPresent() ? current.equals(this.start) || current.equals(this.end.get()) : current.equals(this.start);
    }

    @Override
    public Trigger mutate(double intensity, long seed) {
        int plus = 15 * (int) (MAX_MUTATION * (new Random(seed).nextDouble() * intensity - intensity / 2));
        LocalTime newStart = this.start.plusMinutes(plus);
        Optional<LocalTime> newEnd = this.end.map(t -> t.plusMinutes(plus));
        return new TimeTrigger(newStart, newEnd);
    }

    @Override
    public String toString() {
        return this.start.toString() + (this.end.isPresent() ? " or " + this.end.get().toString() : "");
    }

}
